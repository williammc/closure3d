Closure library and applications
Version 1.0.0

# About Closure 
`Closure` is a fine RGBD Slam library focusing on real-time computer vision tasks and minimal graphics supports.
It is written in C++ and is complied with C++11 standard.
The goals of this library are to be simple, elegant & flexible.

## Version 1.0.0 (October 2014)

# Coding Guideline
`Closure` library and apps follows the C++ Style Guide from [googlecode.com](http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)
- tools: used to check format & raise useful tips (~/tools/cpplint.py)
         (this tools is from the C++ Style Guide resource)
- code formating: check [clang-format](http://clang.llvm.org/docs/ClangFormat.html) tool.

# DEPENDENCIES
## Required dependencies for look3d core libraries
- `OpenCV` (2.4.8+) (cmake needs ${OpenCV_ROOT_DIR} if OpenCVConfig.cmake is not found)
- `OpenFabMap` (included in ~/root/external folder)

## Required dependencies for applications with GUI
- `OpenGL` (2.1+)
- `GLUT` (freeflut for windows)
- `GLEW` (for windows only)
