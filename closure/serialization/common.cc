// Copyright 2014 The Closure3D Authors. All rights reserved.
#include "closure/serialization/common.h"

#include <Eigen/Core>
#include "slick/math/se3.h"

#include "closure/serialization/opencv.h"

#define DECLARE_STREAMABLE_OBJECT_SERIALIZATION_IMPL(StreamableObject)         \
template <class Archive>                                                       \
void serialize(Archive& ar, StreamableObject& p,                               \
              const unsigned int file_version) {                               \
  std::stringstream ss;                                                        \
  ss << p;                                                                     \
  std::string s = ss.str();                                                    \
  ar& make_nvp("streamable_object", s);                                        \
  ss.str("");                                                                  \
  if (Archive::is_loading::value) {                                            \
    ss << s;                                                                   \
    ss >> p;                                                                   \
  }                                                                            \
}

namespace boost {
namespace serialization {

}  // namespace serialization
}  // namespace boost
