#pragma once
#include <memory>
#include <boost/serialization/export.hpp>

#define FORWARD_DECLARE_STRUCT_SHARED_PTR(T)          \
struct T;                              \
using T##Ptr = std::shared_ptr<T>;

#define FORWARD_DECLARE_CLASS_SHARED_PTR(T)          \
class T;                                             \
using T##Ptr = std::shared_ptr<T>;

#define DECLARE_WEAK_PTR(T)            \
struct T;                              \
using T##Ptr = std::weak_ptr<T> T##Ptr;

#define FORWARD_DECLARE_STRUCT_SHARED_PTR_AND_VECTOR(T)          \
struct T;                                         \
using T##Ptr = std::shared_ptr<T>;                \
using T##PtrVector = std::vector<T##Ptr>;


// Serialization ===============================================================
#define END_SERIALIZATION_EXPORT                               \
  BOOST_CLASS_EXPORT_IMPLEMENT(::cv::Mat)                      \

#define CLOSURE_INSTANTIATE_SERIALIZATION_T(T)                         \
template void T::serialize(                                            \
  boost::archive::binary_iarchive&, const unsigned int);               \
template void T::serialize(                                            \
  boost::archive::binary_oarchive&, const unsigned int);               \
                                                                       \
template void T::serialize(                                            \
  boost::archive::text_iarchive&, const unsigned int);                 \
template void T::serialize(                                            \
  boost::archive::text_oarchive&, const unsigned int);

/// used to instantiate specific API for given class T
#if defined(WIN32) && defined(CLOSURE_SHARED_LIBS)
#define CLOSURE_INSTANTIATE_SERIALIZATION(API, T)                       \
template API void T::serialize(                                         \
  boost::archive::binary_iarchive&, const unsigned int);                \
template API void T::serialize(                                         \
  boost::archive::binary_oarchive&, const unsigned int);                \
                                                                        \
template API void T::serialize(                                         \
  boost::archive::text_iarchive&, const unsigned int);                  \
template API void T::serialize(                                         \
  boost::archive::text_oarchive&, const unsigned int);
#else
#define CLOSURE_INSTANTIATE_SERIALIZATION(API, T)
#endif

#if defined(WIN32) && defined(CLOSURE_SHARED_LIBS)
#define CLOSURE_INSTANTIATE_SERIALIZATION_FUNCTION(API, T)              \
template API void serialize(                                            \
  boost::archive::binary_iarchive&, T& obj, const unsigned int);        \
template API void serialize(                                            \
  boost::archive::binary_oarchive&, T& obj, const unsigned int);        \
                                                                        \
template API void serialize(                                            \
  boost::archive::text_iarchive&, T& obj, const unsigned int);          \
template API void serialize(                                            \
  boost::archive::text_oarchive&, T& obj, const unsigned int);
#else
#define CLOSURE_INSTANTIATE_SERIALIZATION_FUNCTION(API, T)
#endif