// Copyright 2014 The Closure3D Authors. All rights reserved.
#include "closure/common/id_register.h"
#include <sstream>

namespace closure {
static IdType incr_index = 0;

IdRegister::IdRegister() {
  Reset();
}

IdRegister::~IdRegister() {}

void IdRegister::Reset(IdType id) {
  incr_index = id;
}

IdType IdRegister::Register() {
  incr_index += 1;
  return incr_index;
}

IdType IdRegister::Next() {
  return incr_index + 1;
}
}  // namespace closure