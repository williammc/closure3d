#pragma once
#include <Eigen/Core>

namespace closure {

using FrameIdx = int;

// single precision types
using ColMat2Xf = Eigen::Matrix<float, 2, Eigen::Dynamic, Eigen::ColMajor>;
using ColMat4Xf = Eigen::Matrix<float, 4, Eigen::Dynamic, Eigen::ColMajor>;
using ColMat6Xf = Eigen::Matrix<float, 6, Eigen::Dynamic, Eigen::ColMajor>;

using Vec2f = Eigen::Matrix<float, 2, 1>;
using Vec3f = Eigen::Matrix<float, 3, 1>;
using Vec4f = Eigen::Matrix<float, 4, 1>;
using Vec6f = Eigen::Matrix<float, 6, 1>;

using Mat2f = Eigen::Matrix<float, 2, 2>;
using Mat3f = Eigen::Matrix<float, 3, 3>;
using Mat4f = Eigen::Matrix<float, 4, 4>;
using Mat6f = Eigen::Matrix<float, 6, 6>;

using Matrix34f = Eigen::Matrix<float, 3, 4>;


using AlignVec2fs = std::vector<Vec2f, Eigen::aligned_allocator<Vec2f>>;
using AlignVec4fs = std::vector<Vec4f, Eigen::aligned_allocator<Vec4f>>;
using AlignVec6fs = std::vector<Vec6f, Eigen::aligned_allocator<Vec6f>>;

// double precision types
using Vec2d = Eigen::Matrix<double, 2, 1>;
using Vec3d = Eigen::Matrix<double, 3, 1>;
using Vec4d = Eigen::Matrix<double, 4, 1>;
using Vec6d = Eigen::Matrix<double, 6, 1>;

using Mat2d = Eigen::Matrix<double, 2, 2>;
using Mat3d = Eigen::Matrix<double, 3, 3>;
using Mat4d = Eigen::Matrix<double, 4, 4>;
using Mat6d = Eigen::Matrix<double, 6, 6>;

using ColMat6Xd = Eigen::Matrix<double, 6, Eigen::Dynamic, Eigen::ColMajor>;

}  // namespace closure