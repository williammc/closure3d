// Copyright 2014 The Closure3D Authors. All rights reserved.
#include "closure/common/debug.h"

namespace closure {
static int nindent = 0;

int get_nindent() { return nindent; }
void increase_nindent() { nindent += 1; }
void decrease_nindent() { nindent -=1; }

void print_indent(std::ostream& os, const int nind) {
  for (int i = 0; i < nind; ++i)
    os << INDENT_GAP;
}

std::string get_indent(const int nind) {
  std::string indent = "";
  for (int i = 0; i < nind; ++i)
    indent = indent + std::string(INDENT_GAP);
  return indent;
}

}  // namespace closure
