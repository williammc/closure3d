// Copyright 2014 The Closure3D Authors. All rights reserved.
#pragma once
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "closure/closure_api.h"
#include "closure/common/macros.h"

namespace closure {

using IdType = int;

struct CLOSURE_API IdRegister {
  IdRegister();
  ~IdRegister();
  
  static void Reset(IdType id = 0);

  /// Register an index value and increate internal index
  static IdType Register();

  /// 'look at' next index value (no update)
  static IdType Next();

  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    IdType id;
    id = Next() - 1;
    ar& id;
    Reset(id);
  }
};
}  // namespace closure
