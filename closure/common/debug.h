// Copyright 2014 The Closure3D Authors. All rights reserved.
#pragma once
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#endif
#include "closure/common/defer.h" 

namespace closure {
  // Userful terminal colors 
#define Closure_COLOR_NORMAL          ""
#define Closure_COLOR_RESET           "\033[m"
#define Closure_COLOR_BOLD            "\033[1m"
#define Closure_COLOR_RED             "\033[31m"
#define Closure_COLOR_GREEN           "\033[32m"
#define Closure_COLOR_YELLOW          "\033[33m"
#define Closure_COLOR_BLUE            "\033[34m"
#define Closure_COLOR_MAGENTA         "\033[35m"
#define Closure_COLOR_CYAN            "\033[36m"
#define Closure_COLOR_BOLD_RED        "\033[1;31m"
#define Closure_COLOR_BOLD_GREEN      "\033[1;32m"
#define Closure_COLOR_BOLD_YELLOW     "\033[1;33m"
#define Closure_COLOR_BOLD_BLUE       "\033[1;34m"
#define Closure_COLOR_BOLD_MAGENTA    "\033[1;35m"
#define Closure_COLOR_BOLD_CYAN       "\033[1;36m"
#define Closure_COLOR_BG_RED          "\033[41m"
#define Closure_COLOR_BG_GREEN        "\033[42m"
#define Closure_COLOR_BG_YELLOW       "\033[43m"
#define Closure_COLOR_BG_BLUE         "\033[44m"
#define Closure_COLOR_BG_MAGENTA      "\033[45m"
#define Closure_COLOR_BG_CYAN         "\033[46m"


// =============================================================================
// Useful macros ===============================================================
#define INDENT_GAP "    "

#if 0 // defined(DEBUG) || defined(_DEBUG)

#define PRINT_CURRENT_SCOPE(NAME_TO_PRINT)                        \
std::cout << get_indent(get_nindent());                           \
increase_nindent();                                               \
std::cout << "T" << GetCurrentThreadId() << "::"                  \
          << "BEGIN " << NAME_TO_PRINT << " in " << __FILE__      \
          << ":" << __LINE__ << std::endl;                        \
auto debug_d = defer([]() {                                       \
    decrease_nindent();                                           \
    std::cout << get_indent(get_nindent())                        \
              << "END " << NAME_TO_PRINT << std::endl;            \
    }                                                             \
);

#define PRINT_FUNC_SCOPE()                                        \
std::cout << get_indent(get_nindent());                           \
increase_nindent();                                               \
std::cout << "T" << GetCurrentThreadId() << "::"                  \
          << "BEGIN " << __FUNCTION__ << " in " << __FILE__       \
            << ":" << __LINE__ << std::endl;                      \
                                                                  \
auto debug_d = defer([]() {                                       \
    decrease_nindent();                                           \
    std::cout << get_indent(get_nindent())                        \
              << "END " << __FUNCTION__ << std::endl;             \
});

#define PRINT_CURRENT_SCOPE_COLORIZED(NAME_TO_PRINT, COLOR)                    \
  std::cout << COLOR << get_indent(get_nindent());                             \
  increase_nindent();                                                          \
  std::cout << "BEGIN " << NAME_TO_PRINT << std::endl;                         \
auto debug_d = defer([]() {                                                    \
    decrease_nindent();                                                        \
    std::cout << get_indent(get_nindent())                                     \
              << "END " << NAME_TO_PRINT << "\e[0m" << std::endl;              \
});

#define INDENT_DEBUG_COUT                                                      \
std::cout << get_indent(get_nindent())

#else
#define PRINT_CURRENT_SCOPE(NAME_TO_PRINT)

#define PRINT_FUNC_SCOPE()

#define PRINT_CURRENT_SCOPE_COLORIZED(NAME_TO_PRINT, COLOR)

#define INDENT_DEBUG_COUT std::cout 
#endif

// =============================================================================
// Useful rountines ============================================================
int get_nindent();
void increase_nindent();
void decrease_nindent();

void print_indent(std::ostream& os, const int nind);
std::string get_indent(const int nind);

}  // namespace closure
