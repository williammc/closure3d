#pragma once
#include "closure/util/common.h"

namespace closure {

// helpful matches drawing given keyframe, list of relocalizer frames, & matches
inline void DrawCorrespondences(cv::Mat query_frame, cv::Mat ref_frame,
                                std::vector<cv::KeyPoint> const& q_keypoints,
                                std::vector<cv::KeyPoint> const& ref_keypoints,
                                std::vector<cv::DMatch> const& goodmatches,
                                std::vector<cv::DMatch> const& allmatches) {
  cv::Mat m_big,m_left,m_right;
  cv::cvtColor(query_frame, m_left, CV_GRAY2RGB);
  cv::cvtColor(ref_frame, m_right, CV_GRAY2RGB);
  CombineMatrixes(m_left,m_right,m_big);
  for (auto& match : goodmatches) {
      unsigned r = std::rand() % 255;
      unsigned g = std::rand() % 255;
      unsigned b = std::rand() % 255;
      cv::Scalar color(r,g,b);
      cv::KeyPoint qkp = q_keypoints[match.queryIdx];
      cv::KeyPoint ref_kp = ref_keypoints[match.trainIdx];

      cv::circle(m_big, qkp.pt, 5, color);
      cv::circle(m_big, cv::Point(ref_kp.pt.x+m_left.cols,ref_kp.pt.y),5, color);
      cv::line(m_big, qkp.pt, cv::Point(ref_kp.pt.x + m_left.cols, ref_kp.pt.y), color);
  }
  char ca_temp[200];
  static int count=0;
  count++;
  sprintf(ca_temp,"testmatches-%i.jpg",count);
  cv::imwrite(ca_temp, m_big);
  sprintf(ca_temp,"testleft-%i.jpg", count);
  cv::imwrite(ca_temp, query_frame);
  sprintf(ca_temp,"testright-%i.jpg", count);
  cv::imwrite(ca_temp, ref_frame);
}
}  // namespace closure