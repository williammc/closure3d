#pragma once
#include <Eigen/Core>
#include <opencv2/features2d/features2d.hpp>
#include "slick/math/se3.h"

namespace closure {

inline Eigen::Affine3d to_affine(slick::SE3d pose) {
  Eigen::Affine3d  aff;
  aff.setIdentity();
  aff.matrix().block<3, 3>(0, 0) = pose.get_rotation().get_matrix();
  aff.matrix().block<3, 1>(0, 3) = pose.get_translation();
  return aff;
}

inline slick::SE3d to_se3(const Eigen::Affine3d& pose) {
  Eigen::Matrix3d rot = pose.matrix().block<3, 3>(0, 0);
  Eigen::Vector3d t = pose.matrix().block<3, 1>(0, 3);
  return slick::SE3d(rot, t);
}

inline bool CreateDetectionMethod(std::string method,
                           std::shared_ptr<cv::FeatureDetector>& detector,
                           std::shared_ptr<cv::DescriptorExtractor>& extractor,
                           std::shared_ptr<cv::DescriptorMatcher>& matcher,
                           const unsigned scales) {
  if (method == "FREAK") {
    detector.reset(new cv::OrbFeatureDetector(500, 1.2f, scales));
    extractor.reset(new cv::FREAK());
  } else if (method == "BRIEF") {
    detector.reset(new cv::OrbFeatureDetector(500, 1.2f, scales));
    extractor.reset(new cv::BriefDescriptorExtractor());
  } else if (method == "ORB") {
    detector.reset(new cv::ORB(500, 1.2f, scales));
    extractor.reset(new cv::ORB(500, 1.2f, scales));
  } else if (method == "BRISK") {
    detector.reset(new cv::BRISK(30, scales));
    extractor.reset(new cv::BRISK(30, scales));
  } else {
    printf("Feature detection method (%s) not supported!", method.c_str());
    return false;
  }

  if (method == "ORB" || method == "BRIEF" || method == "FREAK" ||
      method == "BRISK") {
      matcher.reset(new cv::BFMatcher(cv::NORM_HAMMING2));
  } else {
    matcher.reset(new cv::BFMatcher(cv::NORM_L2));
  }

  return true;
}

// Useful opencv related ======================================================
// C = [A | B]
inline void CombineMatrixes(const cv::Mat &mat1, const cv::Mat &mat2,
                            cv::Mat &mResult) {
  mResult = cv::Mat(std::max<int>(mat1.rows, mat2.rows),
                    mat1.cols + mat2.cols, mat1.type());
  cv::Mat mat1ROI(mResult, cv::Rect(0, 0, mat1.cols, mat1.rows));
  mat1.copyTo(mat1ROI);
  cv::Mat mat2ROI(mResult, cv::Rect(mat1.cols, 0, mat2.cols, mat2.rows));
  mat2.copyTo(mat2ROI);
}
// Utilities for Depth image ===================================================
// Calculates depth histogram (auto figure out depth resolution if given 0)
inline bool CalcDepthHistogram(const cv::Mat& depth_img,
                                 std::vector<float>& depth_hist,
                                 int depth_resolution = 0) {
  if (depth_img.empty())
    return false;
  auto depth_data = (uint16_t*)depth_img.data;
  unsigned int npoints = 0;
  if (depth_resolution == 0) {
    for (int y = 0; y < depth_img.rows; ++y) {
      for (int x = 0; x < depth_img.cols; ++x, ++depth_data) {
        if (*depth_data != 0) {
          depth_resolution = std::max(depth_resolution, int(*depth_data));
        }
      }
    }
  }

  if (depth_resolution == 0)
    return false;

  depth_hist.resize(depth_resolution + 1);
  depth_data = (uint16_t*)depth_img.data;
  for (int y = 0; y < depth_img.rows; ++y) {
    for (int x = 0; x < depth_img.cols; ++x, ++depth_data) {
      if (*depth_data != 0) {
        depth_hist[*depth_data]++;
        npoints++;
      }
    }
  }
  for (int index = 1; index < depth_resolution; index++) {
    depth_hist[index] += depth_hist[index-1];
  }
  if (npoints) {
    for (int index=1; index < depth_resolution; index++) {
      depth_hist[index] = (unsigned int)(256 * (1.0f - (depth_hist[index] / npoints)));
    }
  }
  return true;
}

// Generate histogram image from depth16bit data
inline void GenerateHistogramImage(const cv::Mat& depth16_img,
                                     cv::Mat& histogram_img) {
  if (histogram_img.empty())
    histogram_img.create(depth16_img.rows, depth16_img.cols, CV_8UC3);

  std::vector<float> depth_hist;
  if (!CalcDepthHistogram(depth16_img, depth_hist))
    return;

  auto depth_data = (uint16_t*)depth16_img.data;
  auto hist_data = (uint8_t*)histogram_img.data;
  for (int r = 0; r < histogram_img.rows; ++r) {
    for (int c = 0; c < histogram_img.cols; ++c, ++depth_data) {
      if (*depth_data != 0) {
        *(hist_data++) = 0;
        *(hist_data++) = depth_hist[*depth_data];
        *(hist_data++) = depth_hist[*depth_data];
      } else {
        *(hist_data++) = 0;
        *(hist_data++) = 0;
        *(hist_data++) = 0;
      }
    }
  }
}

// Converts depth image (16bit) to gray image 8bit (kinda compression)
inline void ConvertDepth16bitDepth8bit(const cv::Mat& depth16bit_img,
                                cv::Mat& depth8bit_img) {
  std::vector<float> depth_hist;
  if (depth8bit_img.empty())
    depth8bit_img.create(depth16bit_img.rows,
                         depth16bit_img.cols,
                         CV_8UC1);
  depth8bit_img.setTo(cv::Scalar(0));
  if (!CalcDepthHistogram(depth16bit_img, depth_hist))
    return;
  uint16_t* depth_data = (uint16_t*)depth16bit_img.data;
  for (int r = 0; r < depth8bit_img.rows; ++r) {
    for (int c = 0; c < depth8bit_img.cols; ++c, ++depth_data) {
      if (*depth_data != 0) {
        depth8bit_img.at<uint8_t>(r, c) = depth_hist[*depth_data];
      }
    }
  }
}
}  // namespace closure