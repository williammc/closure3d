// Copyright 2014 The Closure Authors. All rights reserved.
#pragma once
#include <memory>
#include <vector>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/core/core.hpp>

#include "closure/closure_api.h"
#include "closure/common/id_register.h"
#include "closure/common/macros.h"
#include "closure/common/datatypes.h"

namespace closure {
FORWARD_DECLARE_STRUCT_SHARED_PTR(Frame);
FORWARD_DECLARE_STRUCT_SHARED_PTR(PyramidLevelData);

typedef float IntensityType;
typedef float DepthType;

/// Jacobian of (x/z, y/z) = SE3_pose_delta * SE3_pose * (xw, yw, zw) w.r.t SE3_pose_delta
inline void compute_jacobian_uv_wrt_pose(
  const Vec4f& in_frame, Eigen::Matrix<float, 2, 6>& J_pose) {
  const float z_inv = 1.0f / in_frame[2];
  const float x_z_inv = in_frame[0] * z_inv;
  const float y_z_inv = in_frame[1] * z_inv;
  const float cross = x_z_inv * y_z_inv;
  J_pose(0, 0) = J_pose(1, 1) = z_inv;
  J_pose(0, 1) = J_pose(1, 0) = 0;
  J_pose(0, 2) = -x_z_inv * z_inv;
  J_pose(1, 2) = -y_z_inv * z_inv;
  J_pose(0, 3) = -cross;
  J_pose(0, 4) = 1 + x_z_inv*x_z_inv;
  J_pose(0, 5) = -y_z_inv;
  J_pose(1, 3) = -1 - y_z_inv*y_z_inv;
  J_pose(1, 4) = cross;
  J_pose(1, 5) = x_z_inv;
}

/// Jacobian of z = SE3_pose_delta * SE3_pose * (xw, yw, zw) [2] w.r.t SE3_pose_delta
inline void compute_jacobian_z_wrt_pose(
  const Vec4f& p, Vec6f& J_z) {
  J_z(0) = 0.0;
  J_z(1) = 0.0;
  J_z(2) = 1.0;
  J_z(3) = p(1);
  J_z(4) = -p(0);
  J_z(5) = 0.0;
}

/// which type of data used for tracking
struct TrackingDataOption {
  TrackingDataOption() {
    pc_option = PointcloudOption::USE_VALID;
    depth_grad_thres = 0.003f;
    intensity_grad_thres = 10.0f;
  }


  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar& pc_option;
    ar& depth_grad_thres;
    ar& intensity_grad_thres;
  }

  enum PointcloudOption : unsigned {
    USE_VALID = 1 << 0,
    USE_DEPTH_GRAD = 1 << 1,
    USE_INTENSITY_GRAD = 1 << 2,
    USE_ALL = USE_VALID | USE_DEPTH_GRAD | USE_INTENSITY_GRAD
  } pc_option;

  float depth_grad_thres;
  float intensity_grad_thres;
};

/// Used to get necessary tracking data per pyramid level
struct TrackingData {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  TrackingData() = default;
  TrackingData(const TrackingData&) = delete;

  // move construction
  TrackingData(ColMat4Xf& indp, ColMat4Xf&& pc, ColMat4Xf&& grd) {
    intensity_depth = std::move(indp);
    pointcloud = std::move(pc);
    gradient = std::move(grd);
  }

  size_t size() const { return pointcloud.cols(); }

  ColMat4Xf intensity_depth;  ///< intensity, depth
  ColMat4Xf pointcloud;       ///< x, y, z, 1
  ColMat4Xf gradient;          ///< idx, idy, ddx, ddy
};
typedef std::shared_ptr<TrackingData> TrackingDataPtr;

/// Per pyramid-level data
struct CLOSURE_API PyramidLevelData {

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  PyramidLevelData() = default;
  PyramidLevelData(const PyramidLevelData&) = delete;

  PyramidLevelData(const Eigen::Matrix3d& proj,
                   const cv::Mat& intensity, const cv::Mat& depth);

  void Initialize();

  void FillData();

  ColMat4Xf ComputePointCloudTemplate();
  void ComputePointcloudIfEmpty(const ColMat4Xf& template_pc);    ///< pixel-wise 3D points
  void ComputeGradientIfEmpty();       ///< idx, idy, ddx, ddy
  void ComputeGradientIfEmptySSE();       ///< idx, idy, ddx, ddy

  TrackingDataPtr ComputeTrackingDataIfEmpty(const TrackingDataOption);
  TrackingDataPtr ComputeTrackingDataIfEmptySSE(const TrackingDataOption);
  TrackingDataPtr GetTrackingData() const;
  void ReleaseTrackingData();  ///< release heavy memory data

  bool InImage(const float& x, const float& y) const;
  bool InImageWithBorder(const float& x, const float& y, const unsigned border = 0) const;

 protected:
  friend struct Frame;
 
 public:

  size_t width, height;
  Eigen::Matrix3d proj;      ///< projection matrix
  Eigen::Matrix3d inv_proj;  ///< inversed projection matrix

  cv::Mat intensity;         ///< grayscale in float image format
  cv::Mat depth;             ///< depth image in float image format (meter unit)
  cv::Mat rgb;               ///< color image (memory released if not needed)

  ColMat4Xf intensity_depth;  ///< intensity, depth
  ColMat4Xf pointcloud;       ///< pixelwise 3D points
  ColMat4Xf gradient;         ///< idx, idy, ddx, ddy

  
  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);

 protected:
   TrackingDataPtr tracking_data_;
   typedef Eigen::Matrix<float, 2, 6> Matrix26f;
   std::vector<Matrix26f, Eigen::aligned_allocator<Matrix26f>> jacobians_;
};

using PointcloudVec = std::vector < ColMat4Xf, Eigen::aligned_allocator<ColMat4Xf> > ;
struct CLOSURE_API Frame {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  Frame() = default;
  Frame(const Frame&) = delete;

  Frame(const uint32_t id, const double timestamp, const Eigen::Matrix3d& proj,
        const cv::Mat& intensity, const cv::Mat& depth);

  virtual ~Frame();

  void BuildPyramidData(const size_t num_levels);
  void BuildPyramidDataSSE(const size_t num_levels);
  void ReleasePyramidData();   ///< release heavy memory data

  void BuildTrackingData(const TrackingDataOption opt);
  void BuildTrackingDataSSE(const TrackingDataOption opt);
  void ReleaseTrackingData();

  uint32_t id() const { return id_; }
  void set_id(uint32_t id) { id_ = id; }
  void register_id() {
    id_ = (id_ >= 0) ? id_ : IdRegister::Register();
  }

  unsigned width() const { return level(0)->width; }
  unsigned height() const { return level(0)->height; }

  /// Camera to world transformation
  Eigen::Affine3d pose() const { return pose_; }
  void set_pose(const Eigen::Affine3d& pose) { pose_ = pose; }

  const Eigen::Matrix3d& projection() const { return levels_[0]->proj; }

  PyramidLevelDataPtr level(size_t idx) const;

  double timestamp() const;

  // project world point to image plane
  Eigen::Vector2d Project(const Eigen::Vector4d& v4, unsigned l = 0) const {
    const Eigen::Vector4d v4_t = pose_.matrix()*v4;
    const Eigen::Vector3d v3 = level(l)->proj * v4_t.segment<3>(0);
    return Eigen::Vector2d(v3[0], v3[1]);
  }

  // project camera plane onto image plane
  Eigen::Vector2d Project(const Eigen::Vector3d& v3, unsigned l = 0) const {
    auto const v = level(l)->proj * v3;
    return Eigen::Vector2d(v[0], v[1]);
  }

  Eigen::Vector3d UnProject(const Eigen::Vector2d& v2, unsigned l = 0) const {
    return level(l)->inv_proj * Eigen::Vector3d(v2[0], v2[1], 1.0);
  }

  unsigned NumberOfLevels() const {
    return levels_.size();
  }
  
  ColMat4Xf GetPointcloud() const {
    ColMat4Xf pc = TemplatePointcloudVec()[0];
    float* d = (float*) levels_[0]->depth.data;
    for (int idx = 0; idx < TemplatePointcloudVec()[0].cols(); ++idx) {
      pc.col(idx) *= *(d++);
    }
    return pc;
  }

  PointcloudVec& TemplatePointcloudVec() const;
 protected:
  // Properties ---------------------------------------------------------------
  FrameIdx id_;
  double timestamp_;
  Eigen::Affine3d pose_;  ///< camera to world pose of this frame
  std::vector<PyramidLevelDataPtr> levels_;  ///< pyramid levels data

 public:
  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, PyramidLevelData)
CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, Frame)
}  // namespace closure