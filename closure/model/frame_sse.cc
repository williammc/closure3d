#include "closure/model/frame.h"

namespace closure {

// TODO: improve this code from lsd-slam
/// Resize source image to be a halfsize
void HalfsizeFloatImage_SSE(cv::Mat img, cv::Mat& dest_img) {
    dest_img.create(img.rows/2, img.cols/2, CV_32FC1);
    dest_img.setTo(cv::Scalar(0));
    const int width = img.cols;
    const int height = img.rows;
    const float* source = (float*)img.data;
    float* dest = (float*)dest_img.data;

    const int width1 = (width % 8 == 0) ? width : width - width % 8;

    // we leave out the last 1-7 pixels if width is not multiplication of 8
    if (1) {
        __m128 p025 = _mm_setr_ps(0.25f,0.25f,0.25f,0.25f);

        const float* max_y = source + width*height;
        for(const float* y = source; y < max_y; y += width*2) {
            const float* max_x = y + width1;
            for (const float* x = y; x < max_x; x += 8) {
                // i am calculating four dest pixels at a time.

                __m128 top_left = _mm_load_ps((float*)x);
                __m128 bot_left = _mm_load_ps((float*)x + width);
                __m128 left = _mm_add_ps(top_left, bot_left);

                __m128 top_right = _mm_load_ps((float*)x + 4);
                __m128 bot_right = _mm_load_ps((float*)x + width + 4);
                __m128 right = _mm_add_ps(top_right, bot_right);

                __m128 sum1 = _mm_shuffle_ps(left, right, _MM_SHUFFLE(2,0,2,0));
                __m128 sum2 = _mm_shuffle_ps(left, right, _MM_SHUFFLE(3,1,3,1));

                __m128 sum = _mm_add_ps(sum1, sum2);
                sum = _mm_mul_ps(sum, p025);

                _mm_store_ps(dest, sum);
                dest += 4;
            }
        }
        return;
    }

    // backup solution: per pixel calculation
    int wh = width*height;
    const float* s;
    for (int y = 0; y < wh; y += width*2) {
        for (int x = 0; x < width; x += 2) {
            s = source + x + y;
            *dest = (s[0] + s[1] + s[width] + s[1+width]) * 0.25f;
            dest++;
        }
    }
}

void HalfsizeVector_SSE(const ColMat4Xf& input_vec, ColMat4Xf& dest_vec, const int width) {
  dest_vec.resize(Eigen::NoChange, input_vec.cols() / 4);
  const float* source = input_vec.data();
  float* dest = dest_vec.data();

  const int width1 = (width % 2 == 0) ? width : width - width % 2;
  if (1) {
    const float* maxY = source + 4 * input_vec.cols();
    for (const float* y = source; y < maxY; y += 4 * width * 2) {
      const float* maxX = y + 4 * width1;
      for (const float* x = y; x < maxX; x += 8) {
        // four dest pixels at a time.

        __m128 top_left = _mm_load_ps((float*)x);
        __m128 bot_left = _mm_load_ps((float*)x + 4*width);
        __m128 left = _mm_add_ps(top_left, bot_left);

        __m128 top_right = _mm_load_ps((float*)x + 4);
        __m128 bot_right = _mm_load_ps((float*)x + 4*width + 4);
        __m128 right = _mm_add_ps(top_right, bot_right);

        __m128 sumA = _mm_shuffle_ps(left, right, _MM_SHUFFLE(2, 0, 2, 0));
        __m128 sumB = _mm_shuffle_ps(left, right, _MM_SHUFFLE(3, 1, 3, 1));

        __m128 sum = _mm_add_ps(sumA, sumB);
        sum = _mm_mul_ps(sum, _mm_setr_ps(0.25f, 0.25f, 0.25f, 0.25f));

        _mm_store_ps(dest, sum);
        dest += 4;
      }
    }
    return;
  }

  // backup solution: per pixel calculation
  int wh = input_vec.cols();
  const float* s;
  for (int y = 0; y < 4 * wh; y += 4 * width * 2) {
    for (int x = 0; x < 4 * width; x += 8) {
      s = source + x + y;
      for (int i = 0; i < 4; ++i)
        *(dest++) = (s[0 + i] + s[1 + i] + s[4 * width + i] + s[1 + 4 * width + i]) * 0.25f;
      //dest++;
    }
  }
}

// PyramidLevelData implementation ============================================
void PyramidLevelData::ComputeGradientIfEmptySSE() {
  if (gradient.cols() == width * height) return;
  gradient.resize(Eigen::NoChange, width * height);
  gradient.setZero();
  auto compute_derivs_x = [&](const cv::Mat& floatimg, const int row) {
    int idx = 0;
    for (int y = 0; y < floatimg.rows; ++y) {
      for (int x = 0; x < floatimg.cols - 1; ++x, ++idx) {
        const int prex = std::max(x - 1, 0);
        const int posx = std::min(x + 1, floatimg.cols);
        gradient(row, idx) = 0.5f*(floatimg.at<float>(y, posx) -
                                   floatimg.at<float>(y, prex));
      }
      ++idx;
    }
  };

  auto compute_derivs_y = [&](const cv::Mat& floatimg, const int row) {
    int idx = 0;
    for (int y = 0; y < floatimg.rows - 1; ++y) {
      for (int x = 0; x < floatimg.cols; ++x, ++idx) {
        const int prey = std::max(y - 1, 0);
        const int posy = std::min(y + 1, floatimg.cols);
        gradient(row, idx) = 0.5f*(floatimg.at<float>(posy, x) -
                                   floatimg.at<float>(prey, x));
      }
    }
  };

  compute_derivs_x(intensity, 0);
  compute_derivs_y(intensity, 1);
  compute_derivs_x(depth, 2);
  compute_derivs_y(depth, 3);
}

TrackingDataPtr PyramidLevelData::ComputeTrackingDataIfEmptySSE(
  const TrackingDataOption opt) {
  if (tracking_data_) return tracking_data_;

  FillData();
  ComputeGradientIfEmptySSE();

  std::vector<int> valid_ids;
  valid_ids.reserve(width * height);
  if (unsigned(opt.pc_option) & unsigned(TrackingDataOption::USE_INTENSITY_GRAD)) {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) {
        const float d = 0.5*(gradient(0, i) + gradient(1, i));
        if (d >= opt.intensity_grad_thres) valid_ids.push_back(i);
      }
    }
  } else if (unsigned(opt.pc_option) & unsigned(TrackingDataOption::USE_DEPTH_GRAD)) {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) {
        const float d = 0.5*(gradient(2, i) + gradient(3, i));
        if (d >= opt.depth_grad_thres) valid_ids.push_back(i);
      }
    }
  } else {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) valid_ids.push_back(i);
    }
  }

  // collecting pointcloud
  ColMat4Xf pc;
  pc.resize(Eigen::NoChange, valid_ids.size());
  ColMat4Xf valid_indep;
  ColMat4Xf valid_grad;
  valid_indep.resize(Eigen::NoChange, valid_ids.size());
  valid_grad.resize(Eigen::NoChange, valid_ids.size());
  int i = 0;
  for (auto it = valid_ids.begin(); it != valid_ids.end(); ++it, ++i) {
    valid_indep.col(i) = intensity_depth.col(*it);
    pc.col(i) = pointcloud.col(*it);
    valid_grad.col(i) = gradient.col(*it);
  }

  tracking_data_.reset(new TrackingData(std::move(valid_indep),
    std::move(pc),
    std::move(valid_grad)));
  return tracking_data_;
}

// Frame class ================================================================
void Frame::BuildPyramidDataSSE(const size_t num_levels) {
  if (TemplatePointcloudVec().size() < levels_.size()) {
    for (size_t i = TemplatePointcloudVec().size(); i < levels_.size(); ++i) {
      TemplatePointcloudVec().push_back(levels_[i]->ComputePointCloudTemplate());
    }
  }

  for (size_t idx = 0; idx < levels_.size(); ++idx) {
    levels_[idx]->ComputePointcloudIfEmpty(TemplatePointcloudVec()[idx]);  // compute if data released
  }

  if (levels_.size() >= num_levels) return;
  assert(levels_.size() <= TemplatePointcloudVec().size());

  const size_t pre_size = levels_.size();
  const double scale_factor = 2.0;
  for (size_t idx = pre_size; idx < num_levels; ++idx) {
    cv::Mat intensity, depth;
    HalfsizeFloatImage_SSE(levels_[idx - 1]->intensity, intensity);
    HalfsizeFloatImage_SSE(levels_[idx - 1]->depth, depth);
    Eigen::Matrix3d proj1 = levels_[idx - 1]->proj / scale_factor;
    proj1(2, 2) = 1.0;
    levels_.push_back(PyramidLevelDataPtr(new PyramidLevelData(proj1, intensity, depth)));
    if (TemplatePointcloudVec().size() < levels_.size()) {
      TemplatePointcloudVec().push_back(levels_.back()->ComputePointCloudTemplate());
    }
    levels_[idx]->ComputePointcloudIfEmpty(TemplatePointcloudVec()[idx]);
  }
}
}  // namespace closure