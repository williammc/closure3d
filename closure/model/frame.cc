// Copyright 2014 The Closure Authors. All rights reserved.
#include "closure/model/frame.h"
#include <cassert>
#include <fstream>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#ifdef Closure_HAVE_TBB
#include <tbb/parallel_for.h>
#include <tbb/task_scheduler_init.h>
#endif
#include "closure/serialization/serialization.h"

namespace closure {

// PyramidLevelData ===========================================================
PyramidLevelData::PyramidLevelData(const Eigen::Matrix3d& proj,
                  const cv::Mat& in_intensity, const cv::Mat& in_depth)
                  : proj(proj), intensity(in_intensity), depth(in_depth) {
  if (intensity.type() == CV_8UC3 &&
      (depth.type() == CV_16UC1 || depth.type() == CV_16SC1)) {
    rgb = intensity.clone();
    cv::cvtColor(rgb, intensity, CV_RGB2GRAY);
    intensity.convertTo(intensity, CV_32F);

    depth.convertTo(depth, CV_32F);
    depth *= 0.001;  // to meter
  } else {
    assert(intensity.type() == CV_32FC1 && depth.type() == CV_32FC1);
  }

  inv_proj = proj.inverse();
  Initialize();
}

void PyramidLevelData::Initialize() {
  assert(!intensity.empty() || !depth.empty());  // at least one present

  if (!intensity.empty()) {  // make sure 1-channel float image
    assert(intensity.type() == cv::DataType<IntensityType>::type && 
           intensity.channels() == 1);
    width = intensity.cols;
    height = intensity.rows;
  }

  if (!depth.empty()) {  // make sure 1-channel float image
    assert(depth.type() == cv::DataType<DepthType>::type &&
           depth.channels() == 1);
    width = depth.cols;
    height = depth.rows;
  }
}

void PyramidLevelData::FillData() {
  intensity_depth.resize(Eigen::NoChange, width * height);
  auto fill_data = [&](const cv::Mat& floatimg, const int row) {
    int idx = 0;
    for (int y = 0; y < floatimg.rows; ++y) {
      for (int x = 0; x < floatimg.cols; ++x, ++idx) {
        intensity_depth(row, idx) = floatimg.at<float>(y, x);
      }
    }
  };
  if (!intensity.empty())
    fill_data(intensity, 0);
  if (!depth.empty())
    fill_data(depth, 1);
}

ColMat4Xf PyramidLevelData::ComputePointCloudTemplate() {
  ColMat4Xf pc;
  pc.resize(Eigen::NoChange, width * height);
  int idx = 0;
  for (size_t y = 0; y < height; ++y) {
    for (size_t x = 0; x < width; ++x, ++idx) {
      pc(0, idx) = (x - proj(0, 2)) / proj(0, 0);
      pc(1, idx) = (y - proj(1, 2)) / proj(1, 1);
      pc(2, idx) = 1.0f;
      pc(3, idx) = 1.0;
    }
  }
  return std::move(pc);
}

void PyramidLevelData::ComputePointcloudIfEmpty(const ColMat4Xf& template_pc) {
  if (pointcloud.cols() == width * height) return;
  pointcloud.resize(Eigen::NoChange, width * height);

  const float* depth_ptr = depth.ptr<float>();
  int idx = 0;
  for(size_t y = 0; y < height; ++y) {
    for(size_t x = 0; x < width; ++x, ++depth_ptr, ++idx) {
      pointcloud(0, idx) = template_pc(0, idx) * (*depth_ptr);
      pointcloud(1, idx) = template_pc(1, idx) * (*depth_ptr);
      pointcloud(2, idx) = (*depth_ptr);
      pointcloud(3, idx) = 1.0f;
      auto const t = 1.0/pointcloud(2, idx);
      if (((*depth_ptr) != 0 ) && (!std::isfinite(t) || std::isnan(t))) {
        printf("Strange depth value:%f, at (%d, %d)\n", *depth_ptr, x, y);
      }
    }
  }
}

void PyramidLevelData::ComputeGradientIfEmpty() {
  if (gradient.cols() == width * height) return;
  gradient.resize(Eigen::NoChange, width * height);
  gradient.setZero();
  auto compute_derivs_x = [&] (const cv::Mat& floatimg, const int row) {
    int idx = 0;
    for (int y = 0; y < floatimg.rows; ++y) {
      for (int x = 0; x < floatimg.cols - 1; ++x, ++idx) {
        const int prex = std::max(x - 1, 0);
        const int posx = std::min(x + 1, floatimg.cols);
        gradient(row, idx) = 0.5f*(floatimg.at<float>(y, posx) -
                                  floatimg.at<float>(y, prex));
      }
      ++idx;
    }
  };

  auto compute_derivs_y = [&] (const cv::Mat& floatimg, const int row) {
    int idx = 0;
    for (int y = 0; y < floatimg.rows - 1; ++y) {
      for (int x = 0; x < floatimg.cols; ++x, ++idx) {
        const int prey = std::max(y - 1, 0);
        const int posy = std::min(y + 1, floatimg.cols);
        gradient(row, idx) = 0.5f*(floatimg.at<float>(posy, x) -
                                  floatimg.at<float>(prey, x));
      }
    }
  };

  compute_derivs_x(intensity, 0);
  compute_derivs_y(intensity, 1);
  compute_derivs_x(depth, 2);
  compute_derivs_y(depth, 3);
}

TrackingDataPtr PyramidLevelData::ComputeTrackingDataIfEmpty(
  const TrackingDataOption opt) {
  if (tracking_data_) return tracking_data_;

  FillData();
  ComputeGradientIfEmpty();

  std::vector<int> valid_ids;
  valid_ids.reserve(width * height);
  if (unsigned(opt.pc_option) & unsigned(TrackingDataOption::USE_INTENSITY_GRAD)) {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) {
        const float d = 0.5*(gradient(0, i) + gradient(1, i));
        if (d >= opt.intensity_grad_thres) valid_ids.push_back(i);
      }
    }
  } else if (unsigned(opt.pc_option) & unsigned(TrackingDataOption::USE_DEPTH_GRAD)) {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) {
        const float d = 0.5*(gradient(2, i) + gradient(3, i));
        if (d >= opt.depth_grad_thres) valid_ids.push_back(i);
      }
    }
  } else {
    for (int i = 0; i < pointcloud.cols(); ++i) {
      if (pointcloud(2, i) != 0) valid_ids.push_back(i);
    }
  }

  // collecting pointcloud
  ColMat4Xf pc;
  pc.resize(Eigen::NoChange, valid_ids.size());
  ColMat4Xf valid_indep;
  ColMat4Xf valid_grad;
  valid_indep.resize(Eigen::NoChange, valid_ids.size());
  valid_grad.resize(Eigen::NoChange, valid_ids.size());
  int i = 0;
  for (auto it = valid_ids.begin(); it != valid_ids.end(); ++it, ++i) {
    valid_indep.col(i) = intensity_depth.col(*it);
    pc.col(i) = pointcloud.col(*it);
    valid_grad.col(i) = gradient.col(*it);
  }

  tracking_data_.reset(new TrackingData(std::move(valid_indep),
                                        std::move(pc),
                                        std::move(valid_grad)));
  return tracking_data_;
}

TrackingDataPtr PyramidLevelData::GetTrackingData() const {
  return tracking_data_;
}

void PyramidLevelData::ReleaseTrackingData() {
  intensity_depth.resize(Eigen::NoChange, 0);
  pointcloud.resize(Eigen::NoChange, 0);
  gradient.resize(Eigen::NoChange, 0);
  rgb.release();
  tracking_data_.reset();
}

bool PyramidLevelData::InImage(const float& x, const float& y) const {
  return x >= 0 && x < width && y >= 0 && y < height;
}

bool PyramidLevelData::InImageWithBorder(
  const float& x, const float& y, const unsigned border) const {
  return x - border >= 0 && 
         x + border < width &&
         y - border >= 0 &&
         y + border < height;
}

template <class Archive>
void PyramidLevelData::serialize(Archive& ar, const unsigned int version) {
  ar& width;
  ar& height;
  ar& proj;
  ar& inv_proj;

  ar& intensity;
  ar& depth;
  ar& rgb;

  ar& intensity_depth;
  ar& pointcloud;
  ar& gradient;
}

// Frame ======================================================================
static std::vector<ColMat4Xf, Eigen::aligned_allocator<ColMat4Xf>> template_pcs_;  ///< template pointclouds

Frame::Frame(const uint32_t id, const double timestamp, const Eigen::Matrix3d& proj,
            const cv::Mat& intensity, const cv::Mat& depth) {
  id_ = id;
  timestamp_ = timestamp;
  levels_.push_back(PyramidLevelDataPtr(new PyramidLevelData(proj, intensity, depth)));
  pose_.setIdentity();
}

Frame::~Frame() {
}

void Frame::BuildPyramidData(const size_t num_levels) {
  if (template_pcs_.size() < levels_.size()) {
    for (size_t i = template_pcs_.size(); i < levels_.size(); ++i) {
      template_pcs_.push_back(levels_[i]->ComputePointCloudTemplate());
    }
  }

  for (size_t idx = 0; idx < levels_.size(); ++idx) {
    levels_[idx]->ComputePointcloudIfEmpty(template_pcs_[idx]);  // compute if data released
  }

  if (levels_.size() >= num_levels) return;
  assert(levels_.size() <= template_pcs_.size());

  const size_t pre_size = levels_.size();
  const double scale_factor = 2.0;
  for(size_t idx = pre_size; idx < num_levels; ++idx) {
    cv::Mat intensity, depth;
    const cv::Size size(levels_[idx - 1]->intensity.size().width/scale_factor,
                        levels_[idx - 1]->intensity.size().height/scale_factor);
    cv::resize(levels_[idx - 1]->intensity, intensity, size);
    cv::resize(levels_[idx - 1]->depth, depth, size,
               0.0, 0.0, cv::INTER_NEAREST);
    Eigen::Matrix3d proj1 = levels_[idx - 1]->proj / scale_factor;
    proj1(2, 2) = 1.0;
    levels_.push_back(PyramidLevelDataPtr(new PyramidLevelData(proj1, intensity, depth)));
    if (template_pcs_.size() < levels_.size()) {
      template_pcs_.push_back(levels_.back()->ComputePointCloudTemplate());
    }
    levels_[idx]->ComputePointcloudIfEmpty(template_pcs_[idx]);
  }
}

void Frame::ReleasePyramidData() {
  auto level = levels_[0];
  levels_.clear();
  levels_.push_back(level);
}

void Frame::BuildTrackingData(const TrackingDataOption opt) {
  for (auto& level : levels_) 
    level->ComputeTrackingDataIfEmpty(opt);
}

void Frame::ReleaseTrackingData() {
  for (auto& level : levels_) level->ReleaseTrackingData();
}

PyramidLevelDataPtr Frame::level(size_t idx) const {
  assert(idx < levels_.size());

  return levels_[idx];
}

double Frame::timestamp() const {
  return timestamp_;
}

PointcloudVec& Frame::TemplatePointcloudVec() const {
  return template_pcs_;
}

template <class Archive>
void Frame::serialize(Archive& ar, const unsigned int version) {
  ar& id_;
  ar& timestamp_;
  ar& pose_;
  ar& levels_;
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(Frame)
}  // namespace closure