#include "closure/slam/oslam.h"

#include <opencv2/imgproc/imgproc.hpp>

#include "closure/common/id_register.h"
#include "closure/mapping/closure_map.h"
#include "closure/serialization/serialization.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/localizer.h"
#include "closure/tracking/track_reference.h"

namespace closure {
using AlignResult = DenseAligner::AlignResult;

OSlamConfig::OSlamConfig() {
  dcfg.reset(new DenseAlignerConfig());
  lcfg.reset(new LocalizerConfig());
  cm_cfg.reset(new ClosureMapConfig());
}

OSlam::OSlam(const OSlamConfig& cfg) : cfg_(cfg) {
  dense_aligner_.reset(new DenseAligner(*cfg_.dcfg));

  localizer_.reset(new Localizer(*cfg.lcfg));
  localizer_->Signal().connect<OSlam, &OSlam::SwitchKeyframe>(this);

  closure_map_.reset(new closure::ClosureMap(*cfg_.cm_cfg));
  closure_map_->Signal().connect<Localizer, &Localizer::AddKeyframe>(localizer_.get());
  closure_map_->Signal().connect<OSlam, &OSlam::HandleNewKeyframe>(this);

  badtrack_count_ = 0;
  failtrack_count_ = 0;
  frame_count_ = 0;
  switch_kf_.store(false);
  last_ref2c_.setIdentity();
  last_w2c_.setIdentity();
}

OSlam::~OSlam() {
  closure_map_->Signal().disconnect_all();
  localizer_->Signal().disconnect_all();
}

void OSlam::ForceNewKeyframe() {
  closure_map_->ForceNewKeyframe();
}

OSlam::TrackResult OSlam::Track(cv::Mat img, cv::Mat depth,
                                const Eigen::Matrix3d& projection,
                                const double timestamp) {
  current_frame_.reset(new Frame(-1, timestamp , projection, img, depth));

  if (!trk_ref_) {
    auto t = std::chrono::high_resolution_clock::now();
    closure_map_->AddFrame(current_frame_);
    trk_ref_.reset(new TrackReference(current_frame_, *cfg_.dcfg));
    return TrackResult::INIT;
  }

  Eigen::Affine3d ref2frame = last_ref2c_;
  AlignResult res;
  if (0) {
    res = dense_aligner_->Align(trk_ref_, current_frame_, ref2frame);
  } else {
    res = dense_aligner_->AlignSSE(trk_ref_, current_frame_, ref2frame);
  }

  TrackResult tres = TrackResult::GOOD;
  auto update_pose = [&] () {
    last_w2c_ = ref2frame * trk_ref_->frame()->pose().inverse();
    last_ref2c_ = ref2frame;
    badtrack_count_ = 0;
    failtrack_count_ = 0;
    current_frame_->set_pose(last_w2c_.inverse());

    frame_count_ = (frame_count_ > 30) ? 0 : frame_count_ + 1;

    if (tres == TrackResult::GOOD || tres == TrackResult::OK) {
      if (frame_count_ % 10 == 0) {
        Mat6d info = Mat6d::Identity();
        // odormetry constraints
        closure_map_->InsertConstraint(trk_ref_->frame(), current_frame_, ref2frame, info);
      }
      if (frame_count_ % 30 == 0) {
        closure_map_->AddFrame(current_frame_);
      }
    }
  };

  switch (res) {
  case AlignResult::GOOD:
  tres = TrackResult::GOOD;
  update_pose();
  break;
  case AlignResult::OK:
  tres = TrackResult::OK;
  update_pose();
  CheckToSwitchKeyframe(current_frame_);
  break;
  case AlignResult::BAD:
  update_pose();
  badtrack_count_++;
  CheckToSwitchKeyframe(current_frame_);
  break;
  case AlignResult::FAIL:
  failtrack_count_++;
  break;
  }

  if (badtrack_count_ >= 7 || failtrack_count_ > 1) {
    bool ok;
    Eigen::Affine3d w2c_pose;
    std::tie(ok, w2c_pose) = localizer_->Localize(current_frame_);
    if (ok) {
      tres = TrackResult::OK;
      last_w2c_ = w2c_pose;
      last_ref2c_ = last_w2c_ * trk_ref_->frame()->pose();
      badtrack_count_ = 0;
      failtrack_count_ = 0;
    }
  }

  return tres;
}

std::string OSlam::ToString(const TrackResult res) {
  switch (res) {
  case TrackResult::INIT:
    return std::string("INIT");
  case TrackResult::NEW_KF:
    return std::string("NEW_KF");
  case TrackResult::GOOD:
    return std::string("GOOD");
  case TrackResult::OK:
    return std::string("OK");
  case TrackResult::BAD:
    return std::string("BAD");
  case TrackResult::FAIL:
    return std::string("FAIL");
  default:
    return std::string("UNKNOWN");
  }
}

FramePtr OSlam::GetCurrentKeyframe() { 
  return closure_map_->GetLastKeyframe();
}

FramePtr OSlam::GetCurrentFrame() { return current_frame_; }

OSlamConfig& OSlam::Configuration() { return cfg_; }
DenseAlignerConfig& OSlam::DenseAlignerConfiguration() { return *cfg_.dcfg; }
ClosureMapConfig& OSlam::ClosureMapConfiguration() { return *cfg_.cm_cfg; }
LocalizerConfig& OSlam::LocalizerConfiguration() { return *cfg_.lcfg; }

ClosureMap& OSlam::ClosureMap() {
  return *closure_map_;
}

void OSlam::HandleNewKeyframe(FramePtr kf) {
  SwitchKeyframe(kf);
}

void OSlam::SwitchKeyframe(FramePtr frame) {
  switch_kf_.store(true);
  switchto_kf_ = frame;
}

void OSlam::CheckToSwitchKeyframe(FramePtr frame) {
  if (switch_kf_.load()) {
    switch_kf_.store(false);
    trk_ref_.reset(new TrackReference(switchto_kf_, *cfg_.dcfg));
    last_ref2c_ = last_w2c_ * switchto_kf_->pose().inverse();
    return;
  }
  localizer_->ShouldSwitchKeyframe(frame, trk_ref_->frame());
}

template <class Archive>
void OSlam::serialize(Archive& ar, const unsigned int version) {
  ar& cfg_;
  ar& trk_ref_;
  ar& closure_map_;
  ar& localizer_;
  ar& last_w2c_;
  ar& last_ref2c_;
  ar& failtrack_count_;
  ar& badtrack_count_;
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(OSlam)
}  // namespace closure