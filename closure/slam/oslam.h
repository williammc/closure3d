#pragma once
#include <atomic>
#include <memory>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/core/core.hpp>

#include "closure/closure_api.h"
#include "closure/common/datatypes.h"
#include "closure/common/macros.h"
#include "closure/model/frame.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/mapping/closure_map.h"
#include "closure/tracking/localizer.h"

namespace closure {

// forward declare
FORWARD_DECLARE_STRUCT_SHARED_PTR(TrackReference);
FORWARD_DECLARE_CLASS_SHARED_PTR(OSlam);
// ============================================================================
struct CLOSURE_API OSlamConfig {
  OSlamConfig();

  std::shared_ptr<DenseAlignerConfig> dcfg;
  std::shared_ptr<ClosureMapConfig> cm_cfg;
  std::shared_ptr<LocalizerConfig> lcfg;

  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar& dcfg;
    ar& cm_cfg;
    ar& lcfg;
  }
};

/// LOOP closure constraints based SLAM =======================================
/// inshort, O-Slam
class CLOSURE_API OSlam {
 public:
  enum struct TrackResult : unsigned {INIT = 0, NEW_KF, GOOD, OK, BAD, FAIL };

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  OSlam(const OSlam&) = delete;
  OSlam(const OSlamConfig& cfg = OSlamConfig());
  ~OSlam();

  void ForceNewKeyframe();

  /// @img & @depth can be RGB image and Raw 16bit depth  (in milimeter unit)
  /// or both 32bit float image: intensity [0.0f, 255.0f], depth in meter unit
  TrackResult Track(cv::Mat img, cv::Mat depth,
                    const Mat3d& projection, const double timestamp);

  std::string ToString(const TrackResult res);

  Eigen::Affine3d last_w2c_pose() { return last_w2c_; }
  Eigen::Affine3d last_ref2c_pose() { return last_ref2c_; }

  FramePtr GetCurrentKeyframe();
  FramePtr GetCurrentFrame();
  
  OSlamConfig& Configuration();
  DenseAlignerConfig& DenseAlignerConfiguration();
  ClosureMapConfig& ClosureMapConfiguration();
  LocalizerConfig& LocalizerConfiguration();

  ClosureMap& ClosureMap();

  void HandleNewKeyframe(FramePtr);
 protected:
  friend class Localizer;
  /// To be called by Localizer
  void SwitchKeyframe(FramePtr frame);
  ///< consider another keyframe as reference
  void CheckToSwitchKeyframe(FramePtr frame);

  OSlamConfig cfg_;
  TrackReferencePtr trk_ref_;      ///< current keyframe reference for tracking
  DenseAlignerPtr dense_aligner_;  ///< used to align current frame to reference
  ClosureMapPtr closure_map_;    ///< keyframes map handling
  LocalizerPtr localizer_;         ///< used to re-localize when tracking lost

  FramePtr current_frame_;
  Eigen::Affine3d last_w2c_;       ///< last world to camera pose
  Eigen::Affine3d last_ref2c_;

  int failtrack_count_;
  int badtrack_count_;
  int frame_count_;

  std::atomic<bool> switch_kf_;
  FramePtr switchto_kf_;

 public:
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, OSlam)
}  // namespace closure