#include <fstream>
#include <iostream>
#include <tbb/task_scheduler_init.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "closure/model/frame.h"
#include "closure/tracking/track_reference.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/util/common.h"

#include "helpers.h"

using namespace closure;

// Draws desirable target in world coordinate to current color image
void draw_target(cv::Mat& rgb_img, const Matrix34f& proj) {
  const Vec4f point_x(0.1, 0, 1, 1);
  const Vec4f point_y(0, 0.1, 1, 1);
  const Vec4f point_z(0, 0, 1.1, 1);
  const Vec4f point_target(0, 0, 1.0, 1);
  const Vec3f point_cam = proj * point_target;
  const Vec3f pointx_cam = proj * point_x;
  const Vec3f pointy_cam = proj * point_y;
  const Vec3f pointz_cam = proj * point_z;
  cv::line(rgb_img,
           cv::Point(point_cam[0], point_cam[1]),
      cv::Point(pointx_cam[0], pointx_cam[1]),
      cv::Scalar(255, 0, 0),
      3);
  cv::line(rgb_img,
           cv::Point(point_cam[0], point_cam[1]),
      cv::Point(pointy_cam[0], pointy_cam[1]),
      cv::Scalar(0, 255, 0),
      3);
  cv::line(rgb_img,
           cv::Point(point_cam[0], point_cam[1]),
      cv::Point(pointz_cam[0], pointz_cam[1]),
      cv::Scalar(0, 0, 255),
      3);
}

int main () {
  tbb::task_scheduler_init init;  // Automatic number of threads
  //tbb::task_scheduler_init init(tbb::task_scheduler_init::default_num_threads());  // Explicit number of threads

  Eigen::Matrix3d projection;
  projection << 5.2921508098293293e+02, 0.0f, 3.2894272028759258e+02,
                0.0f, 5.2556393630057437e+02, 2.6748068171871557e+02,
                0.0f, 0.0f, 1.0f;
#if 1
  const std::string path_prefix = std::string(Closure_ROOT) +
    "/data/rgbd_sequence";
#else
  const std::string path_prefix = std::string(Closure_ROOT) +
    "/data/rgbd_sequence_long";
#endif
  std::string filename = path_prefix + "/kinect_recorder.txt";
  std::ifstream ifs(filename);
  if (ifs.fail()) {
    printf("Fail to open file: %s\n", filename.c_str());
    return -1;
  }

  std::vector<std::string> color_fns, depth_fns;
  while (!ifs.eof()) {
    std::string tag;
    double timestamp;
    std::string cfn, dfn;
    bool aligned;
    ifs >> tag >> timestamp >> cfn >> dfn >> aligned;
    color_fns.push_back(cfn);
    depth_fns.push_back(dfn);
    printf("Read line color image:%s, depth image:%s\n", cfn.c_str(), dfn.c_str());
  }
  
  const std::string color_fn = path_prefix + "/kinect_recorder_000000-color.png";
  const std::string depth_fn = path_prefix + "/kinect_recorder_000000-depth.png";
  cv::Mat color = cv::imread(color_fn);
  cv::Mat gray_img;
  cv::cvtColor(color, gray_img, CV_BGR2GRAY);
  cv::Mat intensity;
  gray_img.convertTo(intensity, CV_32F);
  cv::Mat depth = cv::imread(depth_fn, -1);
  depth.convertTo(depth, CV_32F);
  depth = depth * 0.001;  // to meter unit

  DenseAlignerConfig cfg(3, 1);
  cfg.depth_grad_thres = 0.003;
  cfg.td_option.pc_option = TrackingDataOption::PointcloudOption::USE_INTENSITY_GRAD;
  cfg.td_option.intensity_grad_thres = 30;
  
  DenseAlignerPtr matcher(new DenseAligner(cfg));
  FramePtr reference, current;
  reference.reset(new Frame(0, 000000, projection, intensity, depth));

  TrackReferencePtr trk_ref(new TrackReference(reference, cfg));

  bool stop = false;
  int index = 0;    

  Eigen::Affine3d ref2cur;
  ref2cur.setIdentity();

  while (!stop) {
    index = (index == color_fns.size() - 1) ? 0 : index + 1;  // loopback
    //if (index == 0) {
    //  ref2cur.setIdentity();
    //}
    //index = 0;
    const std::string color_fn = path_prefix + "/" + color_fns[index];
    const std::string depth_fn = path_prefix + "/" + depth_fns[index];
    cv::Mat color = cv::imread(color_fn);
    cv::Mat gray_img;
    cv::cvtColor(color, gray_img, CV_BGR2GRAY);
    cv::Mat intensity;
    gray_img.convertTo(intensity, CV_32F);
    cv::Mat depth = cv::imread(depth_fn, -1);
    depth.convertTo(depth, CV_32F);
    depth = depth * 0.001;  // to meter unit

    current.reset(new Frame(1, 000000, projection, intensity, depth));

    auto res = matcher->AlignSSE(trk_ref, current, ref2cur);
    std::cout << DenseAligner::ToString(res) << std::endl;

    std::cout << "Tracked pose:\n" << ref2cur.matrix() << std::endl;
    
    Matrix34f proj = (current->projection() * ref2cur.matrix().block<3, 4>(0, 0)).cast<float>();
    cv::Mat bgr_img = color.clone();
    draw_target(bgr_img, proj);

    cv::imshow("Dense Matcher", bgr_img);
    auto const c = cv::waitKey(10);
    switch (c) {
    case 27:
    stop = true;
    break;
    case 'r':
    ref2cur.setIdentity();
    break;
    }
  }

  return 0;
}


