#include <fstream>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "slick/math/se3.h"
#include "closure/model/frame.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/track_reference.h"

#include "config.h"

using namespace closure;

static Eigen::Matrix3d projection;

void generate_warped_images(FramePtr reference,
                            const slick::SE3f& ref2gen,
                            cv::Mat& warped_intensity, cv::Mat& warped_depth) {
  reference->BuildPyramidData(5);
  warped_intensity.create(reference->height(), reference->width(), CV_32FC1);
  warped_intensity.setTo(cv::Scalar(0));
  warped_depth.create(reference->height(), reference->width(), CV_32FC1);
  warped_depth.setTo(cv::Scalar(0));

  int i = 0;
  for (int r = 0; r < reference->height(); ++r) {
    for (int c = 0; c < reference->width(); ++c, ++i) {
      if (reference->level(0)->pointcloud(2, i) == 0) continue;
      Vec3f v3 = ref2gen.get_matrix() * reference->level(0)->pointcloud.col(i);
      v3 = projection.cast<float>() * v3;
      const Vec2f v2(v3[0] / v3[2], v3[1] / v3[2]);
      if (reference->level(0)->InImage(v2[0], v2[1])) {
        const cv::Point pn(v2[0], v2[1]);
        warped_intensity.at<float>(pn) = reference->level(0)->intensity.at<float>(r, c);
        warped_depth.at<float>(pn) = v3[2];
      }
    }
  }
}

int main () {
  projection << 5.2921508098293293e+02, 0.0f, 3.2894272028759258e+02,
                0.0f, 5.2556393630057437e+02, 2.6748068171871557e+02,
                0.0f, 0.0f, 1.0f;

  const std::string data_path = std::string(Closure_ROOT) + "/data/rgbd_sequence";
  const std::string color_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-color.png"; 
  const std::string depth_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-depth.png"; 
  cv::Mat color = cv::imread(color_fn);
  cv::Mat gray_img;
  cv::cvtColor(color, gray_img, CV_BGR2GRAY);
  cv::Mat intensity;
  gray_img.convertTo(intensity, CV_32F);
  cv::Mat depth = cv::imread(depth_fn, -1);
  depth.convertTo(depth, CV_32F);
  depth = depth * 0.001;  // to meter unit

  DenseAlignerConfig cfg(4, 1);
  cfg.depth_grad_thres = 0.003;
  cfg.td_option.pc_option = TrackingDataOption::PointcloudOption::USE_VALID;
  DenseAlignerPtr aligner(new DenseAligner(cfg));
  FramePtr reference, current;
  reference.reset(new Frame(0, 000000, projection, intensity, depth));

  TrackReferencePtr trk_ref(new TrackReference(reference, cfg));
#if 1
  slick::SE3f ref2gen(slick::SO3f(), Vec3f(0.1, 0.01, 0));  // translate 20cm
  // generate 'warped' view for testing
  cv::Mat warped_intensity, warped_depth;
  generate_warped_images(reference, ref2gen, warped_intensity, warped_depth);
  FramePtr gen(new Frame(1, 000001, projection, warped_intensity, warped_depth));
#else
  slick::SE3f ref2gen;  // translate 20cm
  FramePtr gen(new Frame(1, 000001, projection, intensity, depth));
#endif

  Eigen::Matrix<double, 3, 4> gt_pose = ref2gen.get_matrix().cast<double>();
  Eigen::Affine3d ref2gen_est;
  auto printf_diff = [&]() {
    std::cout << "Groundtruth pose:\n" << gt_pose << std::endl;
    std::cout << "Align() pose:\n" << ref2gen_est.matrix() << std::endl;
    std::cout << "Rotation Diff to groudtruth:"
      << (gt_pose.block<3, 3>(0, 0) - ref2gen_est.matrix().block<3, 3>(0, 0)).norm()
      << std::endl;
    const Eigen::Vector3d gt = gt_pose.block(0, 3, 3, 1);
    const Eigen::Vector3d et = ref2gen_est.matrix().block(0, 3, 3, 1);
    std::cout << "Gt translate:" << gt.transpose()
      << " estimated translate:" << et.transpose() << std::endl;
    std::cout << "Translation Diff to groudtruth:" << (gt - et).norm()
              << std::endl;
  };

  ref2gen_est.setIdentity();
  aligner->AlignSSE(trk_ref, gen, ref2gen_est);
  printf_diff();
  return 0;
}


