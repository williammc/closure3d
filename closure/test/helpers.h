#pragma once
#include <opencv2/core/core.hpp>
#include "closure/common/datatypes.h"
#include "closure/model/frame.h"

namespace closure {

inline void generate_warped_images(
  FramePtr reference, const Mat3d& projection, const slick::SE3d& ref2gen,
  cv::Mat& warped_intensity, cv::Mat& warped_depth) {
  reference->BuildPyramidData(5);
  warped_intensity.create(reference->height(), reference->width(), CV_32FC1);
  warped_intensity.setTo(cv::Scalar(0));
  warped_depth.create(reference->height(), reference->width(), CV_32FC1);
  warped_depth.setTo(cv::Scalar(0));

  int i = 0;
  for (int r = 0; r < reference->height(); ++r) {
    for (int c = 0; c < reference->width(); ++c, ++i) {
      if (reference->level(0)->pointcloud(2, i) == 0) continue;
      Vec3d v3 = ref2gen.get_matrix() * reference->level(0)->pointcloud.col(i).cast<double>();
      v3 = projection * v3;
      const Vec2f v2(v3[0] / v3[2], v3[1] / v3[2]);
      if (reference->level(0)->InImage(v2[0], v2[1])) {
        const cv::Point pn(v2[0], v2[1]);
        warped_intensity.at<float>(pn) = reference->level(0)->intensity.at<float>(r, c);
        warped_depth.at<float>(pn) = v3[2];
      }
    }
  }
}


inline void generate_warped_images(
  cv::Mat intensity, cv::Mat depth,
  const Mat3d& projection, const slick::SE3d& ref2gen,
  cv::Mat& warped_intensity, cv::Mat& warped_depth) {
  warped_intensity.create(intensity.rows, intensity.cols, CV_32FC1);
  warped_intensity.setTo(cv::Scalar(0));
  warped_depth.create(intensity.rows, intensity.cols, CV_32FC1);
  warped_depth.setTo(cv::Scalar(0));

  Mat3d inv_proj = projection.inverse();
  auto get_point = [&](int x, int y, float d) {
    Vec3d v = inv_proj * Vec3d(x, y, 1.0) * d;
    return Vec4d(v[0], v[1], v[2], 1.0);
  };

  auto in_image = [&](int x, int y) {
    return (x >= 0 && x < intensity.cols && y >= 0 && y < intensity.rows);
  };

  int i = 0;
  for (int r = 0; r < intensity.rows; ++r) {
    for (int c = 0; c < intensity.cols; ++c, ++i) {
      float d = depth.at<float>(r, c);
      if (d == 0.0f) continue;
      Vec3d v3 = ref2gen.get_matrix() * get_point(c, r, d);
      v3 = projection * v3;
      const Vec2f v2(v3[0] / v3[2], v3[1] / v3[2]);
      if (in_image(v2[0], v2[1])) {
        const cv::Point pn(v2[0], v2[1]);
        warped_intensity.at<float>(pn) = intensity.at<float>(r, c);
        warped_depth.at<float>(pn) = v3[2];
      }
    }
  }
}
}  // namespace closure