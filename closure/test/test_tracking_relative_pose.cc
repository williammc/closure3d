#include <fstream>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "slick/math/se3.h"
#include "closure/model/frame.h"
#include "closure/tracking/relative_pose.h"
#include "closure/util/common.h"

#include "helpers.h"

using namespace closure;

static Eigen::Matrix3d projection;

int main (int argc, const char** argv) {
  std::string detect_method = (argc >1) ? argv[1] : "BRISK";
  projection << 5.2921508098293293e+02, 0.0f, 3.2894272028759258e+02,
                0.0f, 5.2556393630057437e+02, 2.6748068171871557e+02,
                0.0f, 0.0f, 1.0f;

  const std::string data_path = std::string(Closure_ROOT) + "/data/rgbd_sequence";
  const std::string color_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-color.png"; 
  const std::string depth_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-depth.png"; 
  cv::Mat color = cv::imread(color_fn);
  cv::Mat gray_img;
  cv::cvtColor(color, gray_img, CV_BGR2GRAY);
  cv::Mat intensity;
  gray_img.convertTo(intensity, CV_32F);
  cv::Mat depth = cv::imread(depth_fn, -1);

  cv::Mat histogram_img;
  GenerateHistogramImage(depth, histogram_img);
  cv::Mat big_img1;
  CombineMatrixes(histogram_img, color, big_img1);
  cv::imshow("Original depth and color", big_img1);

  depth.convertTo(depth, CV_32F);
  depth = depth * 0.001;  // to meter unit

  slick::SE3 ref2gen(slick::SO3(), Vec3d(0.1, 0.01, 0));  // translate 20cm
  // generate 'warped' view for testing
  cv::Mat warped_intensity, warped_depth;
  generate_warped_images(intensity, depth, projection, ref2gen, 
                         warped_intensity, warped_depth);
  
  std::shared_ptr<cv::FeatureDetector> detector;
  std::shared_ptr<cv::DescriptorExtractor> extractor;
  std::shared_ptr<cv::DescriptorMatcher> matcher;

  if (!CreateDetectionMethod(detect_method, detector, extractor, matcher, 4)) {
    printf("Cannot creat detection method of %s\n", detect_method.c_str());
    return 1;
  }

  cv::Mat warped_gray;
  warped_intensity.convertTo(warped_gray, CV_8U);
  
  Eigen::Affine3d ref2gen_est;
#if 1
  ComputeRelativePose(gray_img, depth, warped_gray, warped_depth,
                      projection, ref2gen_est, detector, extractor, matcher);
#else

  ComputeRelativePose(gray_img, depth, gray_img, depth,
                      projection, ref2gen_est, detector, extractor, matcher);
#endif

  Eigen::Matrix<double, 3, 4> gt_pose = ref2gen.get_matrix();
  auto printf_diff = [&]() {
    std::cout << "Groundtruth pose:\n" << gt_pose << std::endl;
    std::cout << "Calc. relative pose:\n" << ref2gen_est.matrix() << std::endl;
    std::cout << "Rotation Diff to groudtruth:"
      << (gt_pose.block<3, 3>(0, 0) - ref2gen_est.matrix().block<3, 3>(0, 0)).norm()
      << std::endl;
    const Eigen::Vector3d gt = gt_pose.block(0, 3, 3, 1);
    const Eigen::Vector3d et = ref2gen_est.matrix().block(0, 3, 3, 1);
    std::cout << "Gt translate:" << gt.transpose()
      << " estimated translate:" << et.transpose() << std::endl;
    std::cout << "Translation Diff to groudtruth:" << (gt - et).norm()
              << std::endl;
  };

  printf_diff();

  warped_depth = warped_depth * 1000;
  warped_depth.convertTo(warped_depth, CV_16S);
  warped_intensity.convertTo(warped_intensity, CV_8U);
  cv::cvtColor(warped_intensity, warped_intensity, CV_GRAY2BGR);
  GenerateHistogramImage(warped_depth, histogram_img);
  cv::Mat big_img2;
  CombineMatrixes(histogram_img, warped_intensity, big_img2);
  while (cv::waitKey(10) != 27) {
    cv::imshow("Warped depth and color", big_img2);
  }
  return 0;
}