#include <fstream>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "slick/math/se3.h"
#include "closure/common/filesystem.h"
#include "closure/mapping/closure_optimizer.h"
#include "closure/model/frame.h"
#include "closure/serialization/serialization.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/track_reference.h"
#include "closure/util/common.h"

#include "helpers.h"

using namespace closure;

static FramePtr reference;
static cv::Mat warped_intensity, warped_depth;
static Eigen::Matrix3d projection;

std::vector<slick::SE3> gt_poses;
std::vector<FramePtr> frames;
std::vector<slick::SE3> init_poses;
std::vector<slick::SE3> est_poses;

static float noise_magnitude = 0.01f;

void add_noise(Vec3d& v) {
  auto t = std::chrono::high_resolution_clock::now();
  unsigned int s = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
  std::srand(s);
    
  // adding noise
  v[0] *= 1 + float(std::rand() % 100)/100 * noise_magnitude;
  v[1] *= 1 + float(std::rand() % 100) / 100 * noise_magnitude;
  v[2] *= 1 + float(std::rand() % 100) / 100 * noise_magnitude;
}


void generate_gt_frames(int nsamples) {
  for (int i = 0; i < nsamples; ++i) {
    auto t = std::chrono::high_resolution_clock::now();
    unsigned int s = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
    std::srand(s);
    float t1 = float(std::rand() % 100) / 1000;
    float t2 = float(std::rand() % 100) / 1000;
    float t3 = float(std::rand() % 100) / 1000;
    Vec3d gt_t(t1, t2, t3);
    slick::SE3 ref2gen(slick::SO3d(), gt_t);
    gt_poses.push_back(ref2gen);

    // generate 'warped' view for testing
    reference->level(0)->intensity.copyTo(warped_intensity);
    //reference->level(0)->depth.copyTo(warped_depth);
    //generate_warped_images(reference, projection, ref2gen, warped_intensity, warped_depth);

    // adding noise
    add_noise(gt_t);
    ref2gen = slick::SE3(slick::SO3d(), gt_t);
    init_poses.push_back(ref2gen);
    est_poses.push_back(ref2gen);
    FramePtr gen(new Frame(i, 000001, projection, warped_intensity, warped_depth));
    gen->set_pose(closure::to_affine(ref2gen));
    frames.push_back(gen);
  }
}

void printf_diff_pose(int i) {
  std::cout << "Groundtruth pose:\n" << gt_poses[i] << std::endl;
  std::cout << "Init pose:\n" << init_poses[i] << std::endl;
    std::cout << "Estimated pose:\n" << est_poses[i] << std::endl;
    std::cout << "\n==========================\n";
    std::cout << "Init Rotation Diff to groudtruth:"
      << (gt_poses[i].get_rotation().ln() - init_poses[i].get_rotation().ln()).norm() << std::endl;
    std::cout << "Gt translate:" << gt_poses[i].get_translation()
      << " init translate:" << init_poses[i].get_translation() << std::endl;
    std::cout << "Translation Diff to groudtruth:"
      << (gt_poses[i].get_translation() - init_poses[i].get_translation()).norm() << std::endl;

    std::cout << "\n==========================\n";
    std::cout << "Rotation Diff to groudtruth:"
      << (gt_poses[i].get_rotation().ln() - est_poses[i].get_rotation().ln()).norm() << std::endl;
    std::cout << "Gt translate:" << gt_poses[i].get_translation()
      << " estimated translate:" << est_poses[i].get_translation() << std::endl;
    std::cout << "Translation Diff to groudtruth:" 
      << (gt_poses[i].get_translation() - est_poses[i].get_translation()).norm() << std::endl;
  };

int main () {
  projection << 5.2921508098293293e+02, 0.0f, 3.2894272028759258e+02,
                0.0f, 5.2556393630057437e+02, 2.6748068171871557e+02,
                0.0f, 0.0f, 1.0f;

  const std::string data_path = std::string(Closure_ROOT) + "/data/rgbd_sequence";
  const std::string color_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-color.png"; 
  const std::string depth_fn = std::string(Closure_ROOT) + 
                         "/data/rgbd_sequence/kinect_recorder_000000-depth.png"; 
  cv::Mat color = cv::imread(color_fn);
  cv::Mat gray_img;
  cv::cvtColor(color, gray_img, CV_BGR2GRAY);
  cv::Mat intensity;
  gray_img.convertTo(intensity, CV_32F);
  cv::Mat depth = cv::imread(depth_fn, -1);
  depth.convertTo(depth, CV_32F);
  depth = depth * 0.001;  // to meter unit

  reference.reset(new Frame(0, 000000, projection, intensity, depth));
  generate_gt_frames(7);

  ClosureOptimizerPtr optimizer(new ClosureOptimizer);

  printf("generate constraints\n");
  for (int i = 0; i < gt_poses.size()-1; ++i) {
    auto pose1 = gt_poses[i];
    for (int j = i + 1; j < std::min(i + 3, int(gt_poses.size())); ++j) {
      auto pose2 = gt_poses[j];
      slick::SE3d pose1_to_pose2 = pose2.inverse() * pose1;
      Vec3d v = pose1_to_pose2.get_translation();
      add_noise(v);
      pose1_to_pose2 = slick::SE3d(pose1_to_pose2.get_rotation(), v);
      optimizer->InsertConstraint(frames[i].get(), frames[j].get(), 
                                  closure::to_affine(pose1_to_pose2));
    }
  }
  
  printf("do optimization\n");
  optimizer->OptimizeFramePoses();

  printf("collecting results\n");
  auto& lcdata = optimizer->GetLoopClosureData();
  for (int i = 0; i < frames.size(); ++i) {
    double* p = lcdata.FrameParameter(frames[i]->id());
    Vec6d v;
    v << p[0], p[1], p[2], p[3], p[4], p[5];
    est_poses[i] = slick::SE3d(v);
    printf_diff_pose(i);
  }

#if 1  // serialize data
  printf("ClosureOptimizer: save model to file\n");
  std::string data_path1 = std::string(Closure_ROOT) + "/data/20141216";
  std::string fn = "/closure_optimizer_" + std::to_string(lcdata.closure_constraints.size());
  if (CreateDirectory(data_path1))
    SerializeToFile(data_path1, fn, optimizer, FILE_ARCHIVE_BINARY_COMPRESSED);
#endif
  
  return 0;
}


