#include <ceres/jet.h>  // need sqrt() and sin() for SE3Group<Jet>
#include "slick/math/se3.h"

#include "closure/mapping/closure_optimizer.h"
#include <ceres/rotation.h>
#include <ceres/ceres.h>
#include <ceres/loss_function.h>
#include <ceres/solver.h>
#include <ceres/autodiff_cost_function.h>

#include "slick/math/se3.h"
#include "common/filesystem.h"
#include "closure/model/frame.h"
#include "closure/serialization/serialization.h"
#include "closure/util/common.h"

namespace closure {

// Ceres Solver cost functions and problem ====================================
struct SE3Plus {
  template<typename T>
  bool operator()(const T* x, const T* delta, T* x_plus_delta) const {
    using Vec6T = Eigen::Matrix < T, 6, 1 >;
    Vec6T vx;
    vx << x[0], x[1], x[2], x[3], x[4], x[5];
    slick::SE3Group<T> pose_x(vx);
    Vec6T vx1 = pose_x.ln();
    Vec6T v_delta;
    v_delta << delta[0], delta[1], delta[2], delta[3], delta[4], delta[5];
    slick::SE3Group<T> pose_delta(v_delta);
    auto v_plus = (pose_delta * pose_x).ln();
    for (int i = 0; i < 6; ++i)
      x_plus_delta[i] = v_plus[i];

    //std::cout << "vx:" << vx[0] << " " << vx[1] << " " << vx[2] << " "
    //  << vx[3] << " " << vx[4] << " " << vx[5] << " " << std::endl;
    //std::cout << "vx1:" << vx1[0] << " " << vx1[1] << " " << vx1[2] << " "
    //  << vx1[3] << " " << vx1[4] << " " << vx1[5] << " " << std::endl;
    //std::cout << "vdelta:" << v_delta[0] << " " << v_delta[1] << " " << v_delta[2] << " "
    //  << v_delta[3] << " " << v_delta[4] << " " << v_delta[5] << " " << std::endl;
    //std::cout << "vplus:" << v_plus[0] << " " << v_plus[1] << " " << v_plus[2] << " "
    //  << v_plus[3] << " " << v_plus[4] << " " << v_plus[5] << " " << std::endl;
    return true;
  }
};

// Templated pinhole camera model for used with Ceres.  The camera is
// parameterized using 6 parameters: 3 for rotation, 3 for translation.
struct LoopClosureCost {
  LoopClosureCost(const Array6d& cama2camb)
      : observed_cama2camb(cama2camb) {}

  template <typename T>
  bool operator()(const T* const cam_a, const T* const cam_b,
                  T* residuals) const {
#if 1
    using Vec6T = Eigen::Matrix < T, 6, 1 > ;
    Vec6T v_a;
    v_a << cam_a[0], cam_a[1], cam_a[2], cam_a[3], cam_a[4], cam_a[5];
    slick::SE3Group<T> pose_a(v_a);

    Vec6T v_b;
    v_b << cam_b[0], cam_b[1], cam_b[2], cam_b[3], cam_b[4], cam_b[5];
    slick::SE3Group<T> pose_b(v_b);

    auto pose_a2b = pose_b.inverse() * pose_a;
    auto v = pose_a2b.ln();
    for (int i = 0; i < 6; ++i)
      residuals[i] = observed_cama2camb[i] - v[i];

#else
    using Mat3 = Eigen::Matrix < T, 3, 3, Eigen::Unaligned | Eigen::ColMajor> ;
    using Vec3 = Eigen::Matrix < T, 3, 1, Eigen::Unaligned>;
    // rotation at cam_a[3, 4, 5]
    T R_a[9];
    ceres::AngleAxisToRotationMatrix(cam_a + 3, &R_a[0]);
    T R_b[9];
    ceres::AngleAxisToRotationMatrix(cam_b + 3, &R_b[0]);

    // R_a2b = R_b_inv * R_a
    Mat3 R_b_inv = Eigen::Map<Mat3>(R_b).transpose();
    Mat3 R_a2b = R_b_inv * Eigen::Map<Mat3>(R_a);

    // t = R_b_inv*t_a + t_b_inv
    // t_b_inv = -R_b_inv * t_b
    Vec3 t_a2b = -R_b_inv * Vec3(cam_b[0], cam_b[1], cam_b[2]) +
      R_b_inv * Vec3(cam_a[0], cam_a[1], cam_a[2]);  
    
    T rot_a2b[3];
    ceres::RotationMatrixToAngleAxis(R_a2b.data(), &rot_a2b[0]);

#if 1  // test 
    Eigen::Matrix<T, 3, 1> v_ta, v_ra, v_tb, v_rb;
    for (int i = 0; i < 3; ++i) {
      v_ta[i] = cam_a[i];
      v_ra[i] = cam_a[3 + i];
      v_tb[i] = cam_b[i];
      v_rb[i] = cam_b[3 + i];
    }

    slick::SE3Group<T> pose_a(slick::SO3Group<T>(v_ra), v_ta);
    slick::SE3Group<T> pose_b(slick::SO3Group<T>(v_rb), v_tb);

    auto pose_a2b = pose_b.inverse() * pose_a;

    auto diff = pose_a2b.ln();
    std::cout << "pose_a2b:" << diff[0] << " " << diff[1] << " " << diff[2] << " "
      << diff[3] << " " << diff[4] << " " << diff[5] << " " << std::endl;
    std::cout << "est_pose_a2b:" << t_a2b[0] << " " << t_a2b[1] << " " << t_a2b[2] << " "
      << rot_a2b[0] << " " << rot_a2b[1] << " " << rot_a2b[2] << std::endl;
#endif

    for (int i = 0; i < 3; ++i) {
      residuals[i] = T(observed_cama2camb[i]) - t_a2b(i, 0);
      residuals[3 + i] = T(observed_cama2camb[3 + i]) - rot_a2b[i];
    }
#endif
    return true;
  }

  Array6d observed_cama2camb;
};

void build_loopclosure_problem(LoopClosureDataHolder& data,
                                ceres::Problem& problem,
                                ceres::Solver::Options& options) {

  for (auto& cont : data.closure_constraints) {
    // Each Residual block takes a point and a camera as input and outputs a 2
    // dimensional residual. Internally, the cost function stores the observed
    // image location and compares the reprojection against the observation.
    ceres::CostFunction* cost_function =
    new ceres::AutoDiffCostFunction<LoopClosureCost, 6, 6, 6>(
          new LoopClosureCost(cont.frame1_to_frame2_pose));
    problem.AddResidualBlock(cost_function,
                             new ceres::CauchyLoss(0.5), /* squared loss */
                             data.FrameParameter(cont.frame1),
                             data.FrameParameter(cont.frame2));

    
  }
  
  for (auto& tpair : data.frame_index_map) {
    auto param = new ceres::AutoDiffLocalParameterization < SE3Plus, 6, 6 > ;
    problem.SetParameterization(data.FrameParameter(tpair.first), param);
  }

  options.linear_solver_type = ceres::DENSE_SCHUR;
  options.minimizer_progress_to_stdout = true;
}

// LoopClosureDataHolder ======================================================
bool LoopClosureDataHolder::Insert(
  Frame* frame1, Frame* frame2, const Array6d& f1_to_f2_pose) {
  assert(frame1->id() != frame2->id());
  if (!frame_pose_map.count(frame1->id()))
    frame_pose_map[frame1->id()] = to_array(to_se3(frame1->pose()).ln());
  if (!frame_pose_map.count(frame2->id()))
    frame_pose_map[frame2->id()] = to_array(to_se3(frame2->pose()).ln());
  LoopClosureData data{ frame1->id(), frame2->id(), f1_to_f2_pose };
  auto found = std::find(closure_constraints.begin(),
                         closure_constraints.end(), data);
  if (found == closure_constraints.end()) {
    closure_constraints.push_back(data);
    return true;
  }
  return false;
}

bool LoopClosureDataHolder::Insert(FrameIdx f1, FrameIdx f2,
            const Array6d& f1pose, const Array6d& f2pose,
            const Array6d& f1_to_f2_pose) {
  assert(f1 != f2);
  if (!frame_pose_map.count(f1))
    frame_pose_map[f1] = f1pose;
  if (!frame_pose_map.count(f2))
    frame_pose_map[f2] = f2pose;
  LoopClosureData data{ f1, f2, f1_to_f2_pose };
  auto found = std::find(closure_constraints.begin(),
                         closure_constraints.end(), data);
  if (found == closure_constraints.end()) {
    closure_constraints.push_back(data);
    return true;
  }
  return false;
}

void LoopClosureDataHolder::PackParameters() {
  frame_index_map.clear();

  frame_parameters.resize(6 * frame_pose_map.size());
  robust_weights.resize(frame_pose_map.size(), 0);
  int idx = 0;
  for (auto fr : frame_pose_map) {
    for (int i = 0; i < 6; ++i) {
      Array6d v6 = fr.second;
      frame_parameters[idx + i] = v6[i];
    }
    frame_index_map[fr.first] = idx;
    idx += 6;
  }
}

double* LoopClosureDataHolder::FrameParameter(FrameIdx fr) {
  return (double*)&frame_parameters.at(frame_index_map[fr]);
}

void LoopClosureDataHolder::UnPackParameters() {
  for (auto tpair : frame_index_map) {
    auto fr = tpair.first;
    double* p = &frame_parameters.at(tpair.second);
    frame_pose_map[tpair.first] = Array6d{ p[0], p[1], p[2], p[3], p[4], p[5] };
  }
}

void LoopClosureDataHolder::RemoveLoopClosureData(std::unordered_set<FrameIdx> del_frames) {
  std::vector<int> no_del;
  for (size_t i = 0; i < closure_constraints.size(); ++i) {
    auto const& data = closure_constraints[i];
    if (!del_frames.count(data.frame1) &&
        !del_frames.count(data.frame2)) {
      no_del.push_back(i);
    }
  }

  for (auto fid : del_frames) {
    frame_index_map.erase(fid);
    frame_pose_map.erase(fid);
  }

  LoopClosureVec temp;
  for (auto id : no_del) temp.push_back(closure_constraints[id]);
  closure_constraints.swap(temp);
}

template <class Archive>
void LoopClosureDataHolder::serialize(Archive& ar, const unsigned int version) {
  ar& closure_constraints;
  ar& frame_index_map;

  if (Archive::is_loading::value) {
    UnPackParameters();
  }
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(LoopClosureDataHolder)
// ClosureOptimizer ===========================================================
template <class Archive>
void ClosureOptimizerConfig::serialize(Archive& ar, const unsigned int version) {
  ar& threading;
  ar& remove_outlier;
  ar& outlier_weight_threshold;
  ar& max_iteration;
}
CLOSURE_INSTANTIATE_SERIALIZATION_T(ClosureOptimizerConfig)

ClosureOptimizer::ClosureOptimizer(const ClosureOptimizerConfig& cfg)
  : cfg_(cfg) {
}

ClosureOptimizer::~ClosureOptimizer() {
  stop_.store(true);
  if (opt_thread_.joinable())
    opt_thread_.join();
}

void ClosureOptimizer::Clear() {
  std::lock_guard<std::mutex> lock(sync_);
  closure_data_.Clear();
}

void ClosureOptimizer::InsertConstraint(Frame* kf_a, Frame* kf_b,
                                        const Eigen::Affine3d& kfa2kfb) {
  std::lock_guard<std::mutex> lock(sync_);
  closure_data_.Insert(kf_a, kf_b, to_array(to_se3(kfa2kfb).ln()));
}

int ClosureOptimizer::RemoveOutlierConstraints(int nma) {
  std::lock_guard<std::mutex> lock(sync_);

  return 0;
}

void ClosureOptimizer::OptimizeFramePoses(const float limit_seconds) {

  auto looping_func = [&]() {

    // Unpack data
    std::unique_lock<std::mutex> lock(sync_);
    closure_data_.PackParameters();
    lock.unlock();

    // Run optimization
    ceres::Problem problem;
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_SCHUR;
    options.minimizer_progress_to_stdout = true;
    options.max_linear_solver_iterations = 2;
    options.max_num_iterations = 20;
    options.max_solver_time_in_seconds = limit_seconds;
    printf("ClosureOptimizer: prepare and do optimization\n");
    build_loopclosure_problem(closure_data_, problem, options);
#if 0  // serialize data
    printf("ClosureOptimizer: save model to file\n");
    std::string data_path = std::string(Closure_ROOT) + "/data/20141216";
    std::string fn = "/loop_closure_" + std::to_string(closure_data_.closure_constraints.size());
    if (CreateDirectory(data_path))
      SerializeToFile(data_path, fn, closure_data_, FILE_ARCHIVE_BINARY_COMPRESSED);
#endif
    printf("ClosureOptimizer: solve problem ... \n");
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    std::cout << summary.FullReport() << "\n";
    opt_converged_ = summary.termination_type == ceres::CONVERGENCE;

    // Adjust data after optimization
    lock.lock();
    closure_data_.UnPackParameters();
    lock.unlock();

    if (cfg_.remove_outlier)
      RemoveOutlierConstraints();
  };

  if (cfg_.threading)
    opt_thread_ = std::thread(looping_func);
  else
    looping_func();
}

bool ClosureOptimizer::IsConverged() const {
  return opt_converged_;
}

template<typename FrameType>
void ClosureOptimizer::UpdateFramePoses(std::vector<FrameType> frames) {
  printf("ClosureOptimizer::UpdateFramePoses() line:%d\n", __LINE__);
  for (auto fr : frames) {
    printf("ClosureOptimizer::UpdateFramePoses() line:%d\n", __LINE__);
    if (closure_data_.frame_pose_map.count(fr->id())) {
      auto v = closure_data_.frame_pose_map[fr->id()];
      fr->set_pose(to_affine(slick::SE3d(to_vec(v))));
    } else {
      printf("You ask to update frame(id:%d) not in the optimization data\n",
             fr->id());
      printf("Optimization data frames:");
      for (auto id : closure_data_.frame_index_map)
        printf("%d,", id.first);
      std::cout << std::endl;
    }
  }
}

template <class Archive>
void ClosureOptimizer::serialize(Archive& ar, const unsigned int version) {
  printf("ClosureOptimizer::serialize() line:%D\n", __LINE__);
  ar& cfg_;
  ar& signal_;

  std::lock_guard<std::mutex> lock(sync_);
  ar& closure_data_;
}

template void ClosureOptimizer::UpdateFramePoses(std::vector<Frame*> frames);
template void ClosureOptimizer::UpdateFramePoses(std::vector<FramePtr> frames);
CLOSURE_INSTANTIATE_SERIALIZATION_T(ClosureOptimizer)
}  // namespace closure