#pragma once
#include <atomic>
#include <set>
#include <thread>
#include <map>
#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <Eigen/Geometry>
#include <ceres/ceres.h>

#include "signalx/signalx.h"
#include "closure/common/datatypes.h"
#include "closure/common/macros.h"
#include "closure/closure_api.h"

namespace closure {
struct Frame;
using FramePtr = std::shared_ptr<Frame>;
class ClosureOptimizer;
using ClosureOptimizerPtr = std::shared_ptr<ClosureOptimizer>;

using Vec6d = Eigen::Matrix < double, 6, 1 > ;

using Array6d = std::array < double, 6 >;
inline Array6d to_array(const Eigen::Matrix<double, 6, 1>& v) {
  return Array6d{ v[0], v[1], v[2], v[3], v[4], v[5] };
}

inline Vec6d to_vec(const Array6d& ar) {
  Vec6d v;
  v << ar[0], ar[1], ar[2], ar[3], ar[4], ar[5];
  return v;
}

struct LoopClosureDataHolder;

void CLOSURE_API build_loopclosure_problem(LoopClosureDataHolder& data,
                               ceres::Problem& problem,
                               ceres::Solver::Options& options);

/// LoopClosure struct ========================================================
struct CLOSURE_API LoopClosureData {
  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar& frame1;
    ar& frame2;
    ar& frame1_to_frame2_pose;
  }

  FrameIdx frame1;
  FrameIdx frame2;
  Array6d frame1_to_frame2_pose;
};

using LoopClosureVec = std::vector<LoopClosureData>;

inline bool operator == (const LoopClosureData& lhs, const LoopClosureData& rhs) {
  return (lhs.frame1 == rhs.frame1 && lhs.frame2 == rhs.frame2) ||
   (lhs.frame2 == rhs.frame1 && lhs.frame1 == rhs.frame2);
}

// LoopClosureDataHolder ======================================================
struct CLOSURE_API LoopClosureDataHolder {
  void Clear() {
    closure_constraints.clear();
    //frames.clear();
    frame_index_map.clear();
    frame_parameters.clear();
    robust_weights.clear();
  }

  bool Insert(Frame* frame1, Frame* frame2, const Array6d& f1_to_f2_pose);
  bool Insert(FrameIdx f1, FrameIdx f2,
              const Array6d& f1pose, const Array6d& f2pose, 
              const Array6d& f1_to_f2_pose);
  /// unpacking data and layout parameters in memory in efficient way
  void PackParameters();
  double* FrameParameter(FrameIdx frame);  ///< pointing to parameters block
  void UnPackParameters();  ///< update internal parameters after optimization
  void RemoveLoopClosureData(std::unordered_set<FrameIdx> del_frames);

  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);

  // Properties ---------------------------------------------------------------
  // core data
  std::unordered_map<FrameIdx, Array6d> frame_pose_map;
  LoopClosureVec closure_constraints;
  //std::unordered_set<FrameIdx> frames;

  // conventional data for fast accessing and calculation in optimization
  std::unordered_map<FrameIdx, int> frame_index_map;
  /// parameters layout: t1, t2, t3, r1, r2, r3 <=> SE3([t1 t2 t3 r1 r2 r3])
  /// NOTE: it's NOT SE(SO3(r1,r2,r3), [t1 t2 t3])
  std::vector<double> frame_parameters;
  std::vector<double> robust_weights;
};
CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, LoopClosureDataHolder)

// ClosureOptimizer related ===================================================
/// used to configure ClosureOptimizer
struct CLOSURE_API ClosureOptimizerConfig {
  ClosureOptimizerConfig() {
    threading = false;
    remove_outlier = true;
    outlier_weight_threshold = 0.7f;
    max_iteration = 20;
  }

  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);

  bool threading;
  bool remove_outlier;
  float outlier_weight_threshold;
  size_t max_iteration;
};

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, ClosureOptimizerConfig)

// OSlamOptimizer =============================================================
using OptSuccessSignal = sigx::Signal<void(ClosureOptimizer&)>;

/// Loop closure constraints optimization
/// Adjust only camera poses in the pose graph
class CLOSURE_API ClosureOptimizer {
 public:
  ClosureOptimizer(const ClosureOptimizerConfig& cfg = ClosureOptimizerConfig());

  ~ClosureOptimizer();

  ClosureOptimizerConfig& Configuration() { return cfg_; }
  OptSuccessSignal& Signal() { return signal_; }
  LoopClosureDataHolder& GetLoopClosureData() { return closure_data_; }

  // Graph modification -------------------------------------------------------
  void Clear();
  void InsertConstraint(Frame* kfa, Frame* kfb,
                        const Eigen::Affine3d& kfa2kfb);

  void OptimizeFramePoses(const float limit_seconds = 1.0);  ///< can run on another thread
  bool IsConverged() const;
  template<typename FrameType>
  void UpdateFramePoses(std::vector<FrameType> frames);
  int RemoveOutlierConstraints(int nmax = -1);
  
 protected:
  ClosureOptimizerConfig cfg_;
  OptSuccessSignal signal_;

  std::thread opt_thread_;
  std::mutex sync_;
  std::atomic<bool> stop_;
  bool opt_converged_;

  LoopClosureDataHolder closure_data_;
 public:// for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
using ClosureOptimizerPtr = std::shared_ptr < ClosureOptimizer > ;

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, ClosureOptimizer)
}  // namespace closure