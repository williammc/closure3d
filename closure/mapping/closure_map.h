#pragma once
#include <atomic>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include <Eigen/Geometry>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "signalx/signalx.h"
#include "closure/closure_api.h"
#include "closure/common/datatypes.h"
#include "closure/common/defer.h"
#include "closure/common/macros.h"
#include "closure/model/frame.h"

namespace closure {
// forward declare
FORWARD_DECLARE_CLASS_SHARED_PTR(Localizer);
FORWARD_DECLARE_CLASS_SHARED_PTR(ClosureOptimizer);
FORWARD_DECLARE_CLASS_SHARED_PTR(ClosureMap);

struct CLOSURE_API ClosureMapConfig {
  ClosureMapConfig() {
    threading = false;
    max_closure_frames = 5;
    min_closure_frames = 1;
    neighbour_angle_threshold = float(M_PI) * 60 / 180;  // 60 degree
    neighbour_dist_threshold = 1.0f;
    tooclose_angle_threshold = float(M_PI) * 5 / 180;  // 5 degree
    tooclose_dist_threshold = 0.01f;
  }

  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);

  bool threading;               ///< whether to use another thread for the map
  unsigned max_closure_frames;  ///< maximum loop-closure frames allowed
  unsigned min_closure_frames;  ///< minimum loop-closure frames to consider new keyframe

  float neighbour_angle_threshold;  ///< angle offset still consider neighbour
  float neighbour_dist_threshold;   ///< distance offset
  float tooclose_angle_threshold;   ///< for determine if two frames are too close
  float tooclose_dist_threshold;
};
CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, ClosureMapConfig)

using NewKeyframeSignal = sigx::Signal<void(FramePtr)>;
class CLOSURE_API ClosureMap {
 public:
  ClosureMap(const ClosureMapConfig& cfg = ClosureMapConfig());
  ~ClosureMap();

  void ForceNewKeyframe();
  void AddFrame(FramePtr frame);
  void AddNormalFrame(FramePtr frame);

  void InsertConstraint(FramePtr frame1, FramePtr frame2,
    const Eigen::Affine3d& f1tof2_pose,
    const Mat6d& information);

  // used to access keyframes list
  FramePtr GetLastKeyframe();
  std::vector<FramePtr> GetKeyframes();

  ClosureOptimizerPtr GetClosureOptimizer() { return optimizer_; }

  NewKeyframeSignal& Signal() { return signal_; }
 protected:
  void Run();
  void Stop();

  // run in mapping thread
  void ProcessFrame(FramePtr frame); 

  // loop closures constraints related, in mapping thread
  std::vector<FramePtr> SearchAndInsertConstraints(FramePtr);
  bool IsFarEnough(const Eigen::Affine3d& frame1,
                  const Eigen::Affine3d& frame2);

  // in mapping thread
  void OptimizeFramePoses(std::vector<Frame*> frames = std::vector<Frame*>());
  void OptimizeFramePoses(std::vector<FrameIdx> frames);
  int  RemoveOutlierConstraints(double weight_threshold, int n_max = -1);
  void UpdateKeyframePosesFromGraph();
  // Properties ----------------------------------------------------------------
  ClosureMapConfig cfg_;
  ClosureOptimizerPtr optimizer_;
  
  std::vector<FramePtr> keyframes_;
  std::vector<FramePtr> clone_keyframes_;  ///< for external access
  std::mutex clone_sync_;

  NewKeyframeSignal signal_;

  // threading stuffs
  std::atomic<bool> stop_, newframe_, forced_newkf_;
  std::thread mapping_thread_;
  std::mutex ext_sync_;
  std::queue<FramePtr> exter_queue_;  ///< for buffering new frames
  std::queue<FramePtr> inter_queue_;  ///< temporal queue for training

  std::vector<FrameIdx> local_frameids_;
  int local_optimization_count_;
  int global_nonconvergence_count_;
 public:
  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, ClosureMap)
}  // namespace closure