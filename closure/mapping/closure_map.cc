#include "closure/mapping/closure_map.h"
#include <iostream>
#include <thread>

#include "nanoflann/nanoflann.hpp"

#include "closure/common/id_register.h"
#include "closure/mapping/closure_optimizer.h"
#include "closure/model/frame.h"
#include "closure/serialization/serialization.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/localizer.h"
#include "closure/tracking/track_reference.h"

namespace closure {
using TrackResult = DenseAligner::AlignResult;

template <class Archive>
void ClosureMapConfig::serialize(Archive& ar, const unsigned int version) {
  ar& threading;
  ar& max_closure_frames;
  ar& min_closure_frames;
  ar& neighbour_angle_threshold;
  ar& neighbour_dist_threshold;
  ar& tooclose_angle_threshold;
  ar& tooclose_dist_threshold;
}

// Closure Map =================================================================
ClosureMap::ClosureMap(const ClosureMapConfig& cfg) : cfg_(cfg) {
  stop_ = false;
  newframe_ = false;
  optimizer_.reset(new ClosureOptimizer());
  local_optimization_count_ = 0;
  global_nonconvergence_count_ = 0;
  Run();
}

ClosureMap::~ClosureMap() {
  Stop();  // TODO: check why it breaks here (reading access violation)
}

void ClosureMap::Run() {
  auto run_loop = [&]() {
    while (!stop_.load()) {
      std::cout << "looping in mapping" << std::endl;
      if (!newframe_.load()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      } else {
        std::unique_lock<std::mutex> lock(ext_sync_);
        while (!exter_queue_.empty()) {
          inter_queue_.push(exter_queue_.front());
          exter_queue_.pop();
        }
        lock.unlock();

        newframe_.store(false);

        while (inter_queue_.size() > 0) {
          auto kf = inter_queue_.front();
          inter_queue_.pop();
          ProcessFrame(kf);
        }
      }
    }
  };
  if (cfg_.threading) mapping_thread_ = std::thread(run_loop);
}

void ClosureMap::Stop() {
  if (cfg_.threading) {
    stop_.store(true);
    mapping_thread_.join();
  }
}

void ClosureMap::ForceNewKeyframe() {
  newframe_.store(true);
  forced_newkf_.store(true);
}

void ClosureMap::AddFrame(FramePtr kf) {
  if (cfg_.threading) {
    std::lock_guard<std::mutex> lock(ext_sync_);
    exter_queue_.push(kf);
    newframe_.store(true);
  } else {
    ProcessFrame(kf);
  }
}

void ClosureMap::InsertConstraint(FramePtr frame1, FramePtr frame2,
                                  const Eigen::Affine3d& f1tof2_pose,
                                  const Mat6d& information) {
  if (!IsFarEnough(frame1->pose(), frame2->pose())) return;
  frame1->register_id();
  frame2->register_id();
  optimizer_->InsertConstraint(frame1.get(), frame2.get(), f1tof2_pose);
  local_frameids_.push_back(frame1->id());
  local_frameids_.push_back(frame2->id());
}

FramePtr ClosureMap::GetLastKeyframe() {
  std::unique_lock<std::mutex> lock(clone_sync_);
  if (clone_keyframes_.empty()) return FramePtr();
  return clone_keyframes_.back();
}

std::vector<FramePtr> ClosureMap::GetKeyframes() {
  std::unique_lock<std::mutex> lock(clone_sync_);
  return clone_keyframes_;
}

void ClosureMap::ProcessFrame(FramePtr frame) {
  if (keyframes_.empty() || forced_newkf_.load()) {
    frame->register_id();
    keyframes_.push_back(frame);
    std::unique_lock<std::mutex> lock(clone_sync_);
    clone_keyframes_ = keyframes_;
    lock.unlock();
    if (keyframes_.size() > 2) {
      SearchAndInsertConstraints(frame);
    }
    forced_newkf_.store(false);
    signal_(frame);
    return;
  }
  std::vector<FramePtr> valid_neighbours = SearchAndInsertConstraints(frame);

  if (valid_neighbours.empty()) return;

  bool add_kf = (valid_neighbours.size() >= cfg_.min_closure_frames) &&
                (valid_neighbours.size() <= cfg_.max_closure_frames);

  if (add_kf) {
    frame->register_id();
    keyframes_.push_back(frame);
    std::unique_lock<std::mutex> lock(clone_sync_);
    clone_keyframes_ = keyframes_;
    lock.unlock();

    signal_(frame);
  }

  if (local_optimization_count_ < 3) {  // optimize locally
    local_optimization_count_ += 1;
    std::vector<FrameIdx> fids = local_frameids_;
    std::unique(fids.begin(), fids.end());
    local_frameids_.swap(fids);
    OptimizeFramePoses(local_frameids_);
  } else {  // optimize globally
    OptimizeFramePoses();
    local_optimization_count_ = 0;
    local_frameids_.clear();
  }
}

/// used to holde KDTree data
struct KDTreeData {
  KDTreeData()
      : tree(3, *this,
             nanoflann::KDTreeSingleIndexAdaptorParams(10 /* max leaf */)) {}

  using KDTree = nanoflann::KDTreeSingleIndexAdaptor<
      nanoflann::L2_Simple_Adaptor<float, KDTreeData, float>, KDTreeData,
      3  // dim
      >;
  size_t kdtree_get_point_count() const { return locations.size(); }

  float kdtree_distance(const float* p1, const size_t idx_p2,
                        size_t size) const {
    float d = 0.0f;
    const float d2 = p1[2] - locations[idx_p2][2];
    d += d2 * d2;
    const float d1 = p1[1] - locations[idx_p2][1];
    d += d1 * d1;
    const float d0 = p1[0] - locations[idx_p2][0];
    d += d0 * d0;

    return d;
  }

  // Returns the dim'th component of the idx'th point in the class:
  // Since this is inlined and the "dim" argument is typically an immediate
  // value, the
  //  "if/else's" are actually solved at compile time.
  float kdtree_get_pt(const size_t idx, int dim) const {
    if (dim == 0)
      return locations[idx][0];
    else if (dim == 1)
      return locations[idx][1];
    else
      return locations[idx][2];
  }

  // Optional bounding-box computation: return false to default to a standard
  // bbox computation loop.
  //   Return true if the BBOX was already computed by the class and returned in
  //   "bb" so it can be avoided to redo it again.
  //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3
  //   for point clouds)
  template <class BBOX>
  bool kdtree_get_bbox(BBOX& bb) const {
    return false;
  }

  KDTree tree;
  std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f>>
      locations;
};

void FindPossibleConstraints(std::vector<FramePtr> all, FramePtr keyframe,
                             std::vector<FramePtr>& neighbours) {
  KDTreeData kd_data;
  kd_data.locations.reserve(all.size());
  for (auto& kf : all) {
    Eigen::Vector3f p;
    p[0] = kf->pose().translation()(0);
    p[1] = kf->pose().translation()(1);
    p[2] = kf->pose().translation()(2);
    kd_data.locations.push_back(p);
  }

  kd_data.tree.buildIndex();

  Eigen::Vector3f search_point(keyframe->pose().translation()(0),
                               keyframe->pose().translation()(1),
                               keyframe->pose().translation()(2));

  // do a knn search
  const size_t k = 7;
  std::vector<size_t> ret_indices(k);
  std::vector<float> out_dist_sqr(k);
  nanoflann::KNNResultSet<float> result_set(k);
  result_set.init(&ret_indices.front(), &out_dist_sqr.front());
  kd_data.tree.findNeighbors(result_set, search_point.data(),
                             nanoflann::SearchParams(10));
  neighbours.clear();
  for (size_t i = 0; i < std::min(all.size(), ret_indices.size()); ++i) {
    neighbours.push_back(all[ret_indices[i]]);
  }
}

std::vector<FramePtr> ClosureMap::SearchAndInsertConstraints(FramePtr frame) {
  const auto center1 = frame->pose().matrix().block<3, 1>(0, 3);
  const auto z1 = frame->pose().matrix().block<3, 4>(0, 0) *
                  Eigen::Vector4d(0.0, 0.0, 1.0, 1.0);
  // validate matched neighbours
  std::vector<FramePtr> potential_neighbours;
  for (auto kf : keyframes_) {
    const auto center2 = kf->pose().matrix().block<3, 1>(0, 3);
    const auto z2 = kf->pose().matrix().block<3, 4>(0, 0) *
                    Eigen::Vector4d(0.0, 0.0, 1.0, 1.0);
    auto const cos_angle = (z1 - center1).dot(z2 - center2);

    // small angle offset is better for mapping
    if (cos_angle > std::cos(cfg_.neighbour_angle_threshold)) {
      // resonable distance will produce better map
      if ((center1 - center2).norm() < cfg_.neighbour_dist_threshold) {
        potential_neighbours.push_back(kf);
      }
    }
  }

  std::vector<FramePtr> candidates;
  FindPossibleConstraints(potential_neighbours, frame, candidates);

  DenseAlignerConfig cfg(4, 2);
  DenseAligner aligner(cfg);
  TrackReferencePtr ref(new TrackReference(frame, cfg));

  std::vector<FramePtr> final_neighbours;
  for (auto mkf : candidates) {
    // use dense aligner to estimate relative pose

    Eigen::Affine3d ref2frame = mkf->pose().inverse() * ref->frame()->pose();
    auto res = aligner.AlignSSE(ref, mkf, ref2frame);

    // add loop-closure constraint
    if (res == TrackResult::GOOD || res == TrackResult::OK) {
      Mat6d m;
      m.setIdentity();
      InsertConstraint(frame, mkf, ref2frame, m);
      final_neighbours.push_back(mkf);
    }
  }
  printf(
      "ClosureMap::SearchAndInsertConstraints init_nbs:%d, cand:%d, "
      "final_dbs:%d\n",
      int(potential_neighbours.size()), int(candidates.size()),
      int(final_neighbours.size()));
  return final_neighbours;
}

bool ClosureMap::IsFarEnough(const Eigen::Affine3d& frame1,
                             const Eigen::Affine3d& frame2) {
  const auto center1 = frame1.matrix().block<3, 1>(0, 3);
  const auto z1 =
      frame1.matrix().block<3, 4>(0, 0) * Eigen::Vector4d(0.0, 0.0, 1.0, 1.0);
  const auto center2 = frame2.matrix().block<3, 1>(0, 3);
  const auto z2 =
      frame2.matrix().block<3, 4>(0, 0) * Eigen::Vector4d(0.0, 0.0, 1.0, 1.0);
  auto const cos_angle = (z1 - center1).dot(z2 - center2);
  // small angle offset is better for mapping
  return (cos_angle < std::cos(cfg_.tooclose_angle_threshold)) ||
         ((center1 - center2).norm() > cfg_.tooclose_dist_threshold);
}

void ClosureMap::OptimizeFramePoses(std::vector<Frame*> frames) {
  if (frames.empty()) {
    std::cerr << "global optimizing...\n";
    const float limit_seconds =
        5.0f * std::max(1, global_nonconvergence_count_);
    optimizer_->OptimizeFramePoses(limit_seconds);
    optimizer_->UpdateFramePoses(keyframes_);
    if (!optimizer_->IsConverged()) {
      global_nonconvergence_count_ = (global_nonconvergence_count_ > 10)
                                         ? 10
                                         : global_nonconvergence_count_ + 1;
    } else {
      global_nonconvergence_count_ = 0;
    }
    return;
  }

  auto data = optimizer_->GetLoopClosureData();
  auto find_kf = [&](FrameIdx frid) {
    auto f = std::find_if(frames.begin(), frames.end(),
                          [&](Frame* val) { return val->id() == frid; });
    return f != frames.end();
  };

  ClosureOptimizer opt;
  auto& lc_data = opt.GetLoopClosureData();
  for (auto cont : data.closure_constraints) {
    if (find_kf(cont.frame1) || find_kf(cont.frame2)) {
      lc_data.Insert(cont.frame1, cont.frame2, data.frame_pose_map[cont.frame1],
                     data.frame_pose_map[cont.frame2],
                     cont.frame1_to_frame2_pose);
    }
  }

  std::cerr << "selective optimizing..." << std::endl;
  opt.OptimizeFramePoses(3.0f);

  opt.UpdateFramePoses(frames);
}

void ClosureMap::OptimizeFramePoses(std::vector<FrameIdx> frameids) {
  if (frameids.size() < 3) return;

  auto data = optimizer_->GetLoopClosureData();
  auto find_kf = [&](FrameIdx frid) {
    auto f = std::find(frameids.begin(), frameids.end(), frid);
    return f != frameids.end();
  };

  ClosureOptimizer opt;
  auto& lc_data = opt.GetLoopClosureData();
  for (auto cont : data.closure_constraints) {
    if (find_kf(cont.frame1) || find_kf(cont.frame2)) {
      lc_data.Insert(cont.frame1, cont.frame2, data.frame_pose_map[cont.frame1],
                     data.frame_pose_map[cont.frame2],
                     cont.frame1_to_frame2_pose);
    }
  }

  std::cerr << "local optimizing..." << std::endl;
  opt.OptimizeFramePoses();

  std::vector<FramePtr> toupdate_kfs;
  for (auto kf : keyframes_) {
    if (find_kf(kf->id())) {
      toupdate_kfs.push_back(kf);
    }
  }

  opt.UpdateFramePoses(toupdate_kfs);
}

// for serialization
template <class Archive>
void ClosureMap::serialize(Archive& ar, const unsigned int version) {
  ar& cfg_;
  ar& optimizer_;
  ar& keyframes_;
  std::unique_lock<std::mutex> lock(clone_sync_);
  clone_keyframes_ = keyframes_;
  lock.unlock();
  ar& signal_;
  ar& exter_queue_;
  ar& inter_queue_;
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(ClosureMap)
}  // namespace closure