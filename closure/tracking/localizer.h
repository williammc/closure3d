#pragma once
#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

#include <Eigen/Geometry>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "signalx/signalx.h"
#include "slick/math/se3.h"
#include "closure/common/macros.h"
#include "closure/model/frame.h"
#include "closure/closure_api.h"

namespace closure {

struct LocalizerConfig;
FORWARD_DECLARE_STRUCT_SHARED_PTR(LocalizerFrame);
FORWARD_DECLARE_STRUCT_SHARED_PTR(Localizer);

/// Data for localization
struct CLOSURE_API LocalizerFrame {
  LocalizerFrame() = default;
  LocalizerFrame(FramePtr frame);

  void ComputeSmallBlurryImage(const cv::Size small_size, float blur = 2.5f);

  float ZMSSD(LocalizerFrame& other) const;

  FramePtr kf;           ///< Keyframe associates with this frame
  cv::Mat small_blurry;  ///< small blurry image template
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar& kf;
    ar& small_blurry;
  }
};

/// configuration for the localizer
struct CLOSURE_API LocalizerConfig {
  LocalizerConfig() {
    threading = false;
    score_threshold = 0.3;
  }

  float threading;
  float score_threshold;

  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

// Localizer ==================================================================
using ShouldSwitchSignal = sigx::Signal<void(FramePtr)>;

/// used to relocalize frame when tracking is lost
class CLOSURE_API Localizer {
 public:
  Localizer(const Localizer&) = delete;
  Localizer(LocalizerConfig cfg = LocalizerConfig());

  ~Localizer();

  /// add new keyframe
  void AddKeyframe(FramePtr kf);

  /// find best matched fames
  /// @return in descreasing order of matching score
  std::vector<FramePtr> Localizer::Match(FramePtr frame);
  
  /// try to relocalize
  /// @return true if success and transformation from world-to-frame
  std::pair<bool, Eigen::Affine3d> Localize(FramePtr frame);

  /// should we switch current tracking reference from @kf to another keyframe
  /// based on ZMSSD score (smaller is better)
  void ShouldSwitchKeyframe(FramePtr frame, FramePtr kf);
  ShouldSwitchSignal& Signal() { return signal_; }

 protected:
  void Run();   ///< main loop for relocalization thread
  void Stop();  ///< stop the thread

  // detect features from the input frame & do training if necessary
  void Train(FramePtr kf);
  void Clear();

  // Properties ---------------------------------------------------------------
  LocalizerConfig cfg_;

  // for threading stuffs
  std::atomic<bool> stop_;
  std::atomic<bool> newframe_;
  std::thread localizer_thread_;
  std::mutex mutex_external_;
  std::mutex mutex_internal_;

  std::queue<FramePtr> queue_external_;  ///< for buffering new frames
  std::queue<FramePtr> queue_internal_;  ///< temporal queue for training

  // trained frames used for relocalization
  std::vector<std::shared_ptr<LocalizerFrame>> localizer_frames_;

  std::atomic<bool> do_sskf_;
  FramePtr switch_frame_, switch_kf_;
  ShouldSwitchSignal signal_;
 public:
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};  // class Localizer

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, Localizer)
}  // namespace closure