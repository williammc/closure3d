#include "closure/tracking/residual_data.h"
#include <chrono>
#include <iostream>

#ifndef __APPLE__
#include <immintrin.h>
#endif
#include <pmmintrin.h>

#ifdef Closure_HAVE_TBB
#include <tbb/parallel_for.h>
#endif
#include "closure/model/frame.h"

namespace closure {
#define get_sse(val, idx) *(((float *)&val) + idx)

void ResidualData::ComputeResidualsSSE(
  const PyramidLevelDataPtr reference,
  const PyramidLevelDataPtr current,
  const Matrix34f& ref2cur) {
  std::chrono::high_resolution_clock::time_point tv_start, tv_end;
  tv_start = std::chrono::high_resolution_clock::now();

  valid_ids.resize(valid_ids.size(), false);
  residuals.setZero();

  Eigen::Matrix<float, 4, 4, Eigen::RowMajor> m44, mkt44;
  mkt44.setIdentity();
  mkt44.block<3, 4>(0, 0) = current->proj.cast<float>() * ref2cur;
  m44.setIdentity();
  m44.block<3, 4>(0, 0) = ref2cur;
  ColMat4Xf transformed_pc(4, tracking_data->pointcloud.cols());
  ColMat4Xf tproj_pc(4, tracking_data->pointcloud.cols());
  
  std::chrono::high_resolution_clock::time_point tv_start1, tv_end1;
  tv_start1 = std::chrono::high_resolution_clock::now();
  // transforming the pointcloud to current frame
  const int npoints = tracking_data->pointcloud.cols();

  __m128 m_rows[3] = {_mm_load_ps(m44.data()),
                      _mm_load_ps(m44.data() + 4),
                      _mm_load_ps(m44.data() + 8)};

  __m128 mkt_rows[3] = {_mm_load_ps(mkt44.data()),
                        _mm_load_ps(mkt44.data() + 4),
                        _mm_load_ps(mkt44.data() + 8)};

  __m128 lower_bound = _mm_set1_ps(0.0f);
  __m128 upper_bound = _mm_setr_ps(current->width - 2, current->height - 2, 
                                   current->width - 2, current->height - 2);

  // @w0_uuvv : bilinear factors used for x0y0
  // @w1_uuvv : bilinear factors for x1y0
  auto interpolate_vec4f = [&](const __m128 w0_uuvv, const __m128 w1_uuvv,
                               float* x0y0,
                               float* x0y1) -> __m128 {
      // workout weights
      __m128 w0_uuuu = _mm_unpacklo_ps(w0_uuvv, w0_uuvv);
      __m128 w0_vvvv = _mm_unpackhi_ps(w0_uuvv, w0_uuvv);

      __m128 w1_uuuu = _mm_unpacklo_ps(w1_uuvv, w1_uuvv);
      __m128 w1_vvvv = _mm_unpackhi_ps(w1_uuvv, w1_uuvv);

      // interpolate
      __m128 t1 = _mm_mul_ps(w0_vvvv,
          _mm_add_ps(
              _mm_mul_ps(w0_uuuu, _mm_load_ps(x0y0)),
              _mm_mul_ps(w1_uuuu, _mm_load_ps(x0y0 + 4))
          )
      );

      __m128 t2 = _mm_mul_ps(w1_vvvv,
          _mm_add_ps(
              _mm_mul_ps(w0_uuuu, _mm_load_ps(x0y1)),
              _mm_mul_ps(w1_uuuu, _mm_load_ps(x0y1 + 4))
          )
      );

      return _mm_add_ps(t1, t2);  // interpolated Vec4f
  };

  const int npoints1 = (npoints % 2 == 0) ? npoints : npoints - 1;
  for (int i = 0; i < npoints1; i += 2) {
    valid_ids[i] = false;
    valid_ids[i+1] = false;
    // transform and project 2 points at once
    __m128 points[2] = {_mm_load_ps(tracking_data->pointcloud.data() + 4*i),
                        _mm_load_ps(tracking_data->pointcloud.data() + 4*i + 4)};
    __m128 p1_xyz[3], p2_xyz[3];
    const int mask = ~0; // 0x55;
    float* tpc = transformed_pc.data() + 4 * i;
    for (int j = 0; j < 3; ++j) {
      p1_xyz[j] = _mm_mul_ps(mkt_rows[j], points[0]);
      p2_xyz[j] = _mm_mul_ps(mkt_rows[j], points[1]);

      // transform points
      __m128 res = _mm_dp_ps(m_rows[j], points[0], mask);
      *(tpc + j) = get_sse(res, 0);
      res = _mm_dp_ps(m_rows[j], points[1], mask);
      *(tpc + j + 4) = get_sse(res, 0);
    }

    __m128 x1y1_x2y2 = _mm_hadd_ps(_mm_hadd_ps(p1_xyz[0], p1_xyz[1]),
                                   _mm_hadd_ps(p2_xyz[0], p2_xyz[1]));
    __m128 z1z1_z2z2 = _mm_hadd_ps(_mm_hadd_ps(p1_xyz[2], p1_xyz[2]),
                                   _mm_hadd_ps(p2_xyz[2], p2_xyz[2]));
    __m128 u1v1_u2v2 = _mm_mul_ps(x1y1_x2y2, _mm_rcp_ps(z1z1_z2z2));

    // floor 
    __m128 u1v1_u2v2_floor = _mm_floor_ps(u1v1_u2v2);
    __m128i u1v1_u2v2_floori = _mm_cvtps_epi32(u1v1_u2v2_floor);

    // bilinear interpolation factors
    __m128 p1f11_p2f11 = _mm_sub_ps(u1v1_u2v2, u1v1_u2v2_floor);
    __m128 p1f00_p2f00 = _mm_sub_ps(_mm_set1_ps(1.0f), p1f11_p2f11);
    __m128 p1w0_uuvv = _mm_unpacklo_ps(p1f00_p2f00, p1f00_p2f00);
    __m128 p1w1_uuvv = _mm_unpacklo_ps(p1f11_p2f11, p1f11_p2f11);
    __m128 p2w0_uuvv = _mm_unpackhi_ps(p1f00_p2f00, p1f00_p2f00);
    __m128 p2w1_uuvv = _mm_unpackhi_ps(p1f11_p2f11, p1f11_p2f11);

    // workout image bounds
    int u1v1_u2v2_bounds = _mm_movemask_ps(
                             _mm_and_ps(_mm_cmpge_ps(u1v1_u2v2, lower_bound),
                                        _mm_cmple_ps(u1v1_u2v2, upper_bound)));

    EIGEN_ALIGN16 int u1v1_u2v2_int[4];
    _mm_store_si128((__m128i*) u1v1_u2v2_int, u1v1_u2v2_floori);

    const float fx = current->proj(0, 0);
    const float fy = current->proj(1, 1);

    auto pixel_index = [&](const int x, const int y) {
      return y * current->width + x;
    };

    auto calc_residual = [&](const int pidx, 
                             const __m128 interp_indep, 
                             const __m128 interp_grad) {
      Vec4f ref_indep = tracking_data->intensity_depth.col(pidx);
      ref_indep(1) = transformed_pc(2, pidx); // v4[2];

      const float factor = 1.0f / 255.0f;
      // used to compute compact data (efficiency)
      __m128 cufactor = _mm_setr_ps(0.5f * fx * factor, 0.5f * fy * factor, fx, fy);
      __m128 reffactor = _mm_setr_ps(0.5f * fx * factor, 0.5f * fy * factor, 0.0f, 0.0f);

      float* grad = warped_gradient.data() + 4 * pidx;
      __m128 ref_grad = _mm_load_ps(tracking_data->gradient.col(pidx).data());
      _mm_store_ps(warped_gradient.data() + 4 * pidx, 
                   _mm_add_ps(_mm_mul_ps(ref_grad, reffactor), 
                              _mm_mul_ps(interp_grad, cufactor)));

      valid_ids[pidx] = true;
      warped_pointcloud.col(pidx) = tracking_data->pointcloud.col(pidx); // v4;
      residuals.col(pidx) = Vec2f(interp_indep.m128_f32[0], interp_indep.m128_f32[1]) - ref_indep.head<2>();
      residuals(0, pidx) *= factor;
    };

    // checking on 1st point
    if ((u1v1_u2v2_bounds & 3) == 3) {

      // interpolated gradients
      auto interp_grad = interpolate_vec4f(
        p1w0_uuvv, p1w1_uuvv,
        current->gradient.data() + 4 * pixel_index(u1v1_u2v2_int[0], u1v1_u2v2_int[1]),
        current->gradient.data() + 4 * pixel_index(u1v1_u2v2_int[0], u1v1_u2v2_int[1] + 1));

      // calculate interpolated warped intensity depth, warped gradients
      auto interp_indep = interpolate_vec4f(
        p1w0_uuvv, p1w1_uuvv,
        current->intensity_depth.data() + 4*pixel_index(u1v1_u2v2_int[0], u1v1_u2v2_int[1]),
        current->intensity_depth.data() + 4*pixel_index(u1v1_u2v2_int[0], u1v1_u2v2_int[1] + 1));


      calc_residual(i, interp_indep, interp_grad);
    }

    // checking on 2nd point
    if ((u1v1_u2v2_bounds & 12) == 12 && (i+1 < current->intensity_depth.cols() - 1)) {
      // calculate interpolated warped intensity depth, warped gradients
      auto interp_indep = interpolate_vec4f(
        p2w0_uuvv, p2w1_uuvv,
        current->intensity_depth.data() + 4*pixel_index(u1v1_u2v2_int[2], u1v1_u2v2_int[3]),
        current->intensity_depth.data() + 4*pixel_index(u1v1_u2v2_int[2], u1v1_u2v2_int[3] + 1));
      _mm_store_ps(warped_indep.col(i).data(), interp_indep);

      // interpolated gradients
      auto interp_grad = interpolate_vec4f(
        p2w0_uuvv, p2w1_uuvv,
        current->gradient.data() + 4*pixel_index(u1v1_u2v2_int[2], u1v1_u2v2_int[3]),
        current->gradient.data() + 4*pixel_index(u1v1_u2v2_int[2], u1v1_u2v2_int[3] + 1));

      calc_residual(i + 1, interp_indep, interp_grad);
    }
  }

  tv_end1 = std::chrono::high_resolution_clock::now();
  auto mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end1 - tv_start1).count();
  printf("ComputeResiduals transform took:%dmilliseconds\n", mls);

  tv_end1 = std::chrono::high_resolution_clock::now();
  mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end1 - tv_start1).count();
  printf("ComputeResiduals main step took:%dmilliseconds\n", mls);

  tv_end = std::chrono::high_resolution_clock::now();
  mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
  printf("ComputeResiduals took:%dmilliseconds\n", mls);
}

void ResidualData::ComputeWeightsSSE(const PyramidLevelDataPtr reference,
                        const PyramidLevelDataPtr current,
                        const Mat2f& cov) {
  auto tv_start = std::chrono::high_resolution_clock::now();
  int count_valid = 0;
  auto calc_sigma = [&](float median_sq) -> float {
    float sigma = 1.4826f *
      (1.0f + 5.0f / (count_valid * 2.0f - 6.0f)) * sqrt(median_sq);
    sigma = 1.345f * sigma;
    return sigma * sigma;
  };

  weights.setZero();

  __m128 ms = _mm_set1_ps(0.0f);
  float* rs = residuals.data();
  for (int i = 0; i < valid_ids.size()-1; i += 2) {
    auto valid = _mm_setr_ps(int(valid_ids[i]), int(valid_ids[i]),
                                   int(valid_ids[i+1]), int(valid_ids[i+1]));
    valid = _mm_cmpeq_ps(valid, _mm_set1_ps(1.0));
    auto const res = _mm_load_ps(rs + 2 * i);
    ms = _mm_add_ps(ms, _mm_and_ps(valid, res));
    count_valid += int(valid_ids[i]) + int(valid_ids[i+1]);
  }

  Vec2f mean_residual(ms.m128_f32[0] + ms.m128_f32[2], 
                      ms.m128_f32[1] + ms.m128_f32[3]);

  mean_residual /= count_valid;

  auto sq_sigma1 = calc_sigma(mean_residual[0] * mean_residual[0]);
  auto sq_sigma2 = calc_sigma(mean_residual[1] * mean_residual[1]);

  weights.setZero();

  __m128 ones = _mm_set1_ps(1.0f);
  __m128 zeros = _mm_set1_ps(0.0f);

  __m128 sum = zeros;

  __m128 sq_sigma = _mm_setr_ps(sq_sigma1, sq_sigma2, sq_sigma1, sq_sigma2);
  auto calc_weight_sse = [&](__m128 r) -> __m128 {
    __m128 sq_r = _mm_mul_ps(r, r);
    // (sq_r == 0) ? 0 : 1/sq_r => check isfinite number
    __m128 ceq = _mm_cmpeq_ps(sq_r, zeros);
    __m128 inv_sq_r = _mm_or_ps(_mm_and_ps(ceq, zeros),
                                _mm_andnot_ps(ceq, _mm_rcp_ps(sq_r)));
    // std::sqrt(sq_sigma / sq_r);
    __m128 t = _mm_sqrt_ps(_mm_mul_ps(sq_sigma, inv_sq_r));
    // weight = (sq_r < sq_sigma) ? 1 : std::sqrt(sq_sigma / sq_r);
    __m128 weight = _mm_cmplt_ps(sq_r, sq_sigma);
    weight = _mm_or_ps(_mm_and_ps(weight, ones),
                       _mm_andnot_ps(weight, t));
    sum = _mm_add_ps(sum, _mm_mul_ps(weight, sq_r));
    return weight;
  };

  for (int i = 0; i < valid_ids.size()-1; i +=2) {
    auto valid = _mm_setr_ps(int(valid_ids[i]), int(valid_ids[i]),
                             int(valid_ids[i + 1]), int(valid_ids[i + 1]));
    valid = _mm_cmpeq_ps(valid, _mm_set1_ps(1.0));
    auto const res = _mm_load_ps(residuals.data() + 2 * i);
    __m128 weight = calc_weight_sse(_mm_and_ps(valid, res));
    _mm_store_ps(weights.data() + 2 * i, weight);
  }

  // horizontal sum
  sum = _mm_hadd_ps(sum, sum);
  sum = _mm_hadd_ps(sum, sum);
  float sum_sq_error = sum.m128_f32[0];

  last_error = sum_sq_error / count_valid;
  last_valid_count = count_valid;
  
  auto tv_end = std::chrono::high_resolution_clock::now();
  auto mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
  printf("ComputeWeights took:%dmilliseconds\n", mls);
}
}  // namespace closure