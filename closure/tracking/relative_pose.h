#pragma once
#include <functional>
#include <iostream>

#include <opencv2/features2d/features2d.hpp>

#include "slick/math/se3.h"
#include "slick/scene/three_point_pose.h"
#include "slick/util/common.h"
#include "slick/util/mestimator.h"

#include "closure/debug/localizer_debug.h"
#include "closure/common/datatypes.h"
#include "closure/model/frame.h"
#include "closure/util/common.h"

namespace closure {

inline Vec4d GetWorldPosition(unsigned x, unsigned y, cv::Mat depth, 
  const Mat3d& inv_proj) {
  assert(x >=0 && x < depth.cols && y >=0 && y < depth.rows);
  const unsigned pixel_idx = x*depth.cols + y;
  Vec3d v = inv_proj * Vec3d(x, y, 1.0);
  v *= depth.at<float>(y, x);
  return Vec4d(v[0], v[1], v[2], 1.0);
}

template <typename P, int O1, int O2>
inline Eigen::Matrix<P, 2, 1> transform_and_project(
  const  slick::SE3Group<P>& pose, const Eigen::Matrix<P, 4, 1, O1>& x,
  Eigen::Matrix<P, 2, 6, O2>& J_pose) {
  const Eigen::Matrix<P, 4, 1> in_frame = pose * x;

  const P z_inv = 1.0 / in_frame[2];
  const P x_z_inv = in_frame[0] * z_inv;
  const P y_z_inv = in_frame[1] * z_inv;
  const P cross = x_z_inv * y_z_inv;
  J_pose(0, 0) = J_pose(1, 1) = z_inv * in_frame[3];
  J_pose(0, 1) = J_pose(1, 0) = 0;
  J_pose(0, 2) = -x_z_inv * z_inv * in_frame[3];
  J_pose(1, 2) = -y_z_inv * z_inv * in_frame[3];
  J_pose(0, 3) = -cross;
  J_pose(0, 4) = 1 + x_z_inv*x_z_inv;
  J_pose(0, 5) = -y_z_inv;
  J_pose(1, 3) = -1 - y_z_inv*y_z_inv;
  J_pose(1, 4) = cross;
  J_pose(1, 5) = x_z_inv;

  return Eigen::Matrix<P, 2, 1>(x_z_inv, y_z_inv);
}

template<typename Scalar>
inline std::pair<slick::SE3Group<Scalar>, Scalar>
  optimise_pose(const std::vector<std::pair<Eigen::Matrix<Scalar, 4, 1>,
  Eigen::Matrix<Scalar, 2, 1> > > & observations,
  slick::SE3Group<Scalar> pose, std::vector<bool>& inliers,
  const Scalar threshold) {
  inliers.resize(observations.size());
  unsigned counter = 0;
  Eigen::Matrix<Scalar, 6, 6> JTJ;
  JTJ.setZero();
  Eigen::Matrix<Scalar, 6, 1> JTE;
  JTE.setZero();
  Eigen::Matrix<Scalar, 2, 6> J_pose;
  Eigen::Matrix<Scalar, 6, 1> delta;
  const Eigen::Matrix<Scalar, 2, 2> W = Eigen::Matrix<Scalar, 2, 2>::Identity();
  std::vector<Scalar> sq_errors;
  double sum_squared_error = 0;
  for (int iter = 0; iter < 10; ++iter) {
    // calculate sigma
    sq_errors.clear();
    for (unsigned i = 0; i < observations.size(); ++i) {
      const Eigen::Matrix<Scalar, 4, 1> v4 = pose * observations[i].first;
      const Eigen::Matrix<Scalar, 2, 1> v2_error =
        observations[i].second - 
        Eigen::Matrix<Scalar, 2, 1>(v4[0] / v4[2], v4[1] / v4[2]);
      sq_errors.push_back(v2_error.squaredNorm());
    }

    slick::Tukey<Scalar> est;
    est.compute_sigma_squared(sq_errors);
    sum_squared_error = 0.0;
    // do optimisation
    for (unsigned i = 0; i < observations.size(); ++i) {
      const Eigen::Matrix<Scalar, 2, 1> v2_error =
        observations[i].second - transform_and_project(pose, observations[i].first, J_pose);
      const Scalar e = v2_error.squaredNorm();
      const Scalar w = est.weight(e);
      sum_squared_error += e*w;
      if (w > threshold) {
        inliers[i] = true;
      } else {
        inliers[i] = false;
      }
      Eigen::Matrix<Scalar, 2, 2> Ww = W*w;
      JTJ += J_pose.transpose() * Ww * J_pose;
      JTE += J_pose.transpose() * Ww * v2_error;
    }
    
    delta = Eigen::LDLT<Eigen::Matrix<Scalar, 6, 6>>(JTJ).solve(JTE);

    pose = slick::SE3Group<Scalar>(delta) * pose;
    ++counter;
    if (delta.squaredNorm() <= 1e-9) break;
  }
  return std::make_pair(pose, sum_squared_error / observations.size());
}

/// @frame1_gray & @frame2_gray must be CV_8UC1 type
inline std::pair<bool, float> ComputeRelativePose(
  cv::Mat frame1_gray, cv::Mat frame1_depth,
  cv::Mat frame2_gray, cv::Mat frame2_depth,
  const Mat3d& projection,
  Eigen::Affine3d& ref2frame,
  std::shared_ptr<cv::FeatureDetector> detector,
  std::shared_ptr<cv::DescriptorExtractor> extractor,
  std::shared_ptr<cv::DescriptorMatcher> matcher) {
  assert(frame1_gray.type() == CV_8U && frame2_gray.type() == CV_8U);

  // on reference frame
  std::vector<cv::KeyPoint> frame1_keypoints;
  detector->detect(frame1_gray, frame1_keypoints);
  std::cout << "detected keypoints:" << frame1_keypoints.size() << std::endl;

  cv::Mat frame1_desc;
  extractor->compute(frame1_gray, frame1_keypoints, frame1_desc);

  // on dest frame
  std::vector<cv::KeyPoint> frame2_keypoints;
  detector->detect(frame2_gray, frame2_keypoints);
  std::cout << "detected keypoints:" << frame2_keypoints.size() << std::endl;

  cv::Mat frame2_desc;
  extractor->compute(frame2_gray, frame2_keypoints, frame2_desc);

  std::vector<cv::DMatch> allmatches;
  matcher->match(frame2_desc, frame1_desc, allmatches);

  // backward matching
  std::vector<cv::DMatch> matches;
  matches.reserve(2);
  std::vector<cv::DMatch> back_matches;
  for (auto& m : allmatches) {
    const cv::Mat& train_descriptor = frame1_desc.row(m.trainIdx);
    matches.clear();
    matcher->match(train_descriptor, frame2_desc, matches);
    if (matches[0].trainIdx == m.queryIdx)
      back_matches.push_back(m);
  }
  std::cout << "merry matches:" << back_matches.size() << std::endl;
  allmatches.swap(back_matches);

  float maxdist = 0; float mindist = 100;
  for (std::vector<cv::DMatch>::size_type i = 0; i < allmatches.size(); i++) {
    float dist = allmatches[i].distance;
    if (dist < mindist ) mindist = dist;
    if (dist > maxdist ) maxdist = dist;
  }

  std::vector< cv::DMatch > goodmatches;
  for (std::vector<cv::DMatch>::size_type i = 0; i < allmatches.size(); i++ ) {
    if (allmatches[i].distance < 3*mindist || allmatches[i].distance == 0)
      goodmatches.push_back( allmatches[i]);
  }

#if 1
  DrawCorrespondences(frame2_gray, frame1_gray, frame2_keypoints, frame1_keypoints,
                      goodmatches, allmatches);
#endif
  
  if (goodmatches.size() < 10)
    return std::make_pair(false, 1.e+9);

  std::vector<std::pair<Vec4d, Vec2d> > observations;
  auto const inv_proj = projection.inverse();
  for (auto& m : goodmatches) {
    auto kp = frame1_keypoints[m.trainIdx];
    const Vec4d pw = GetWorldPosition(kp.pt.x, kp.pt.y, frame1_depth, inv_proj);
    if (pw[3] != 0.0) {
      kp = frame2_keypoints[m.queryIdx];
      Vec3d v3(kp.pt.x, kp.pt.y, 1.0);
      observations.push_back(std::make_pair(pw, slick::project(inv_proj * v3)));
    }
  }

  if (observations.size() < 20) return std::make_pair(false, 1.e+9);

  try {
    // robust absolute pose
    std::pair<bool, slick::SE3d> res =
        slick::ComputeRobustAbsolutPoseRANSACMLE(observations);
    if (!res.first)
      return std::make_pair(false, 1.e+9);
    // optimise pose
    std::vector<bool> inliers;
    std::pair<slick::SE3d, double> pair_re =
        optimise_pose<double>(observations, res.second, inliers, 0.8);
    ref2frame = to_affine(pair_re.first);
    return std::make_pair(true, pair_re.second);
  }catch (std::exception &e) {
    printf("Oops! %s\n", e.what());
    return std::make_pair(false, 1.e+9);
  }
}

}  // namespace closure