#pragma once
#include <memory>
#include <string>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include "closure/closure_api.h"
#include "closure/common/macros.h"
#include "closure/model/frame.h"

namespace closure {
FORWARD_DECLARE_STRUCT_SHARED_PTR(Frame);
FORWARD_DECLARE_STRUCT_SHARED_PTR(TrackReference)
FORWARD_DECLARE_STRUCT_SHARED_PTR(DenseAligner);

struct CLOSURE_API DenseAlignerConfig {
  DenseAlignerConfig(const DenseAlignerConfig&) = default;
  DenseAlignerConfig(int startl = 4, int endl = 0) {
    assert(startl <= 6);
    assert(endl >= 0);
    assert(startl >= endl);
    debug = true;
    start_level = startl;
    end_level = endl;
    depth_grad_thres = 0.003f;   // 3mm
    intensity_grad_thres = 10.0f;  // 10 in range [0, 255]

    convergence_delta.resize(start_level + 1);
    max_itrations.resize(start_level + 1);
    for (int i = 0; i <= start_level; ++i) {
      convergence_delta[i] = 1.e-9;
      max_itrations[i] = (i < 4) ? 5 * (1 + i) : 50;
    }
    inlier_threshhold = 0.7f;
  }


  // for serialization
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version) {
    ar& debug;
    ar& start_level;
    ar& end_level;
    ar& td_option;
    ar& depth_grad_thres;
    ar& intensity_grad_thres;

    ar& convergence_delta;
    ar& max_itrations;
    ar& inlier_threshhold;
  }

  // Tracking settings
  bool debug;                    ///< whether to print debug output
  int start_level;               ///< start level at higher levels
  int end_level;                 ///< which level we stop
  TrackingDataOption td_option;  ///< options for selecting poins to track
  float depth_grad_thres;        ///< which depth gradient to take
  float intensity_grad_thres;   ///< which intensity gradient to take

  /// least square optimization settings
  std::vector<float> convergence_delta;   ///< stop when delta is too small
  std::vector<int> max_itrations;         ///< maximum number of iterations
  float inlier_threshhold;                ///< used to determine inlier
};

// Dense aligner class ========================================================
class CLOSURE_API DenseAligner {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  enum struct AlignResult : uint32_t { GOOD = 0, OK, BAD, FAIL };
  DenseAligner() = delete;
  DenseAligner(const DenseAligner&) = delete;
  DenseAligner(const DenseAlignerConfig& cfg);

  static std::string ToString(const AlignResult res);
  AlignResult Align(TrackReferencePtr trk_ref, FramePtr current,
                    Eigen::Affine3d& ref2cur);

  AlignResult AlignSSE(TrackReferencePtr trk_ref, FramePtr current,
                       Eigen::Affine3d& ref2cur);

  float last_inlier_ratio() { return last_inlier_ratio_; }
  float last_track_ratio() { return last_track_ratio_; }

 protected:
   DenseAlignerConfig cfg_;
   float last_inlier_ratio_;
   float last_track_ratio_;
  
};

}  // namespace closure