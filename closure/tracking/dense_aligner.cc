#include "closure/tracking/dense_aligner.h"
#include <cassert>
#include <cassert>
#include <chrono>
#include <chrono>
#include <iostream>
#include <iostream>
#include <tbb/parallel_for.h>
#include "slick/math/se3.h"
#include "closure/model/frame.h"
#include "closure/tracking/residual_data.h"
#include "closure/tracking/track_reference.h"

namespace closure {

  DenseAligner::DenseAligner(const DenseAlignerConfig& cfg) {
    assert(cfg.end_level >= 0);
    assert(cfg.end_level <= cfg.start_level);
    cfg_ = cfg;
  }

  std::string DenseAligner::ToString(const AlignResult res) {
    switch (res) {
    case AlignResult::GOOD:
      return std::string("GOOD");
    case AlignResult::OK:
      return std::string("OK");
    case AlignResult::BAD:
      return std::string("BAD");
    case AlignResult::FAIL:
      return std::string("FAIL");
    default:
      return std::string("UNKNOWN");
    }
  }


  DenseAligner::AlignResult DenseAligner::Align(
    TrackReferencePtr trk_ref, FramePtr current, Eigen::Affine3d& ref2cur) {

    std::chrono::high_resolution_clock::time_point tv_start, tv_end;
    if (cfg_.debug) {
      tv_start = std::chrono::high_resolution_clock::now();
    }

    current->BuildPyramidData(cfg_.start_level + 1);

    assert(trk_ref->frame()->NumberOfLevels() > 0);
    assert(trk_ref->frame()->NumberOfLevels() == current->NumberOfLevels());

    const Mat3f mrot = ref2cur.matrix().block<3, 3>(0, 0).cast<float>();
    const Vec3f t = ref2cur.matrix().block<3, 1>(0, 3).cast<float>();
    slick::SO3f rot(mrot);
    slick::SE3f pose(rot, t);
    std::vector<std::vector<float>> avg_errors;
    avg_errors.resize(cfg_.start_level + 1);
    last_inlier_ratio_ = 0.0f;
    last_track_ratio_ = 0.0f;
    float last_avg_error = 0.0f;
    for (int level = cfg_.start_level; level >= cfg_.end_level; --level) {
      auto reflvl = trk_ref->frame()->level(level);
      auto curlvl = current->level(level);
      curlvl->ComputeTrackingDataIfEmpty(cfg_.td_option);

      ReferenceDataOnLevel& ref_data = trk_ref->GetRefData(reflvl);
      ResidualData& rd = *trk_ref->GetRefData(reflvl).rd;

      Mat2f cov;
      cov.setIdentity();
      rd.ComputeResidualsAndWeights(reflvl, curlvl, cov,
                                    pose.get_matrix().cast<float>());

      Vec6f delta;
      std::vector<float> iter_avg_errors;
      float pre_avg_error = std::numeric_limits<float>::max();
      for (int iter = 0; iter < cfg_.max_itrations[level]; ++iter) {
        Mat6f JTJ = Mat6f::Zero();
        Vec6f JTE = Vec6f::Zero();

        const int nmeas = rd.residuals.cols();
        std::vector<Mat6f, Eigen::aligned_allocator<Mat6f>> JTJs(nmeas);
        std::vector<Vec6f, Eigen::aligned_allocator<Vec6f>> JTEs(nmeas);
        
        Eigen::Matrix<float, 4, 4, Eigen::RowMajor> m44;
        m44.setIdentity();
        m44.block<3, 4>(0, 0) = pose.get_matrix();
        tbb::parallel_for(
          tbb::blocked_range<size_t>(0, nmeas),
          [&](tbb::blocked_range<size_t> r) {
          for (size_t i = r.begin(); i != r.end(); ++i) {
            if (!rd.valid_ids[i]) continue;
            Mat2f W = Mat2f::Identity();
            W(0, 0) = rd.weights(0, i);
            W(1, 1) = rd.weights(1, i);

            Eigen::Matrix<float, 2, 6> J_pose;
            Vec6f J_z;
            const Vec4f v4 = rd.warped_pointcloud.col(i);
            compute_jacobian_uv_wrt_pose(v4, J_pose);
            compute_jacobian_z_wrt_pose(v4, J_z);

            Eigen::Matrix<float, 2, 6> J = Eigen::Matrix<float, 2, 6>::Zero();
            J.row(0) = - rd.warped_gradient.col(i).segment<2>(0).transpose() * J_pose;
            
#if 0  // ESM
            Vec6f J_z1;
            compute_jacobian_z_wrt_pose(rd.inter_pointcloud[i], J_z1);
            J.row(1) = - (rd.warped_compact[i].segment<2>(4).transpose() * J_pose 
                          - 0.5f*(J_z.transpose() + J_z1.transpose()));
#else
            J.row(1) = -(rd.warped_gradient.col(i).segment<2>(2).transpose() * J_pose
            - J_z.transpose());

#endif
            cov.noalias() += J * J.transpose();
            JTJs[i] = (J.transpose() * W) * J;
            JTEs[i] = (J.transpose() * W) * rd.residuals.col(i);
          }
        });

        for (int i = 0; i < nmeas; ++i) {
          if (!rd.valid_ids[i]) continue;
          JTJ += JTJs[i];
          JTE += JTEs[i];
        }
        delta = Eigen::LDLT<Mat6f >(JTJ).solve(JTE);

        auto const pre_pose = pose;
        pose = slick::SE3f(delta) * pose;

        rd.ComputeResidualsAndWeights(
          reflvl, curlvl, cov, pose.get_matrix().cast<float>());
        
        if (rd.last_error > pre_avg_error) {
          pose = pre_pose;
          break;
        }
        pre_avg_error = rd.last_error;

        if (cfg_.debug) {
          std::cout << "iteration: " << iter
            << " rd.last_error:" << rd.last_error
            << " pose:\n" << pose.get_matrix() << std::endl;
        }
        iter_avg_errors.push_back(rd.last_error);

        if (delta.squaredNorm() <= cfg_.convergence_delta[level])
          break;
      }
      last_inlier_ratio_ = rd.ComputeInlierRatio(cfg_.inlier_threshhold);
      last_track_ratio_ = rd.ComputeTrackRatio();
      last_avg_error = pre_avg_error;

      avg_errors[level] = iter_avg_errors;
      if (cfg_.debug) {
        printf("avg errors:");
        for (auto e : iter_avg_errors) printf("%f,", e);
        printf("\n");
      }
    }

    if (cfg_.debug) {
      tv_end = std::chrono::high_resolution_clock::now();
      auto const mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
      printf("DenseAligner::Align() took:%dmilliseconds\n", mls);
      printf("last_inlier_ratio_:%f\n", last_inlier_ratio_);
      printf("last_track_ratio_:%f\n", last_track_ratio_);
    }
    ref2cur.matrix().block<3, 4>(0, 0) = pose.get_matrix().cast<double>();

    if (last_inlier_ratio_ > 0.7f && last_avg_error < 1.e-3)
      return AlignResult::GOOD;
    if (last_inlier_ratio_ > 0.5f && last_avg_error < 3.e-3)
      return AlignResult::OK;
    if (last_inlier_ratio_ > 0.5f && last_avg_error < 1.e-2)
      return AlignResult::BAD;
    return AlignResult::FAIL;
  }
  
}  // namespace closure