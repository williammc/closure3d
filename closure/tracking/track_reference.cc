#include "closure/tracking/track_reference.h"
#include "closure/serialization/serialization.h"
#include "closure/tracking/residual_data.h"

namespace closure {
// ReferenceDataOnLevel =======================================================
ReferenceDataOnLevel::ReferenceDataOnLevel(PyramidLevelDataPtr lvl) {
  rd.reset(new ResidualData(lvl));
  const int nmeas = rd->residuals.cols();
  JTJs.resize(nmeas);
  JTEs.resize(nmeas);
}

void ReferenceDataOnLevel::Reset() {
  for (auto& m : JTJs) m.SetZero();
  for (auto& v : JTEs) v.setZero();
  rd->Reset();
}

// TrackReference =============================================================
TrackReference::TrackReference(FramePtr frame, DenseAlignerConfig cfg) 
  : frame_(frame), track_cfg_(cfg) {
  BuildTrackingData();
}

void TrackReference::Reset() {
  for (auto& tpair : lvl_ref_map_) {
    tpair.second.Reset();
  }
}

void TrackReference::BuildTrackingData() {
  frame_->BuildPyramidData(track_cfg_.start_level + 1);
  for (int level = track_cfg_.start_level; level >= track_cfg_.end_level; --level) {
    auto reflvl = frame_->level(level);
    reflvl->ComputeTrackingDataIfEmpty(track_cfg_.td_option);
    lvl_ref_map_[reflvl] = ReferenceDataOnLevel(reflvl);
  }
}

template <class Archive>
void TrackReference::serialize(Archive& ar, const unsigned int version) {
  ar& frame_;
  ar& track_cfg_;
  if (Archive::is_loading::value) {
    BuildTrackingData();
  }
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(TrackReference)
}  // namespace closure