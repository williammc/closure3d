#pragma once
#include <unordered_map>
#include "closure/common/macros.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/math_sse.h"
#include "closure/closure_api.h"

namespace closure {

// forward declare
FORWARD_DECLARE_STRUCT_SHARED_PTR(Frame);
FORWARD_DECLARE_STRUCT_SHARED_PTR(PyramidLevelData);
FORWARD_DECLARE_STRUCT_SHARED_PTR(TrackingData);
FORWARD_DECLARE_STRUCT_SHARED_PTR(ResidualData);
FORWARD_DECLARE_STRUCT_SHARED_PTR(TrackReference);

/// Used to cache data per pyramid level of the reference keyframe
struct ReferenceDataOnLevel {
  ReferenceDataOnLevel() = default;
  ReferenceDataOnLevel(PyramidLevelDataPtr lvl);

  void Reset();

  PyramidLevelDataPtr lvl;
  ResidualDataPtr rd;
  // cached Jacobians
  std::vector<OptMat6f, Eigen::aligned_allocator<OptMat6f>> JTJs;
  std::vector<Vec6f, Eigen::aligned_allocator<Vec6f>> JTEs;
};

/// Used to build up tracking reference keyframe
/// For efficiently in memory allocation and data handling
struct CLOSURE_API TrackReference {
  TrackReference() = default;
  TrackReference(const TrackReference&) = delete;
  TrackReference(FramePtr frame, DenseAlignerConfig cfg);

  void Reset();
  
  ReferenceDataOnLevel& GetRefData(PyramidLevelDataPtr lvl) {
    return lvl_ref_map_[lvl];
  }

  FramePtr frame() {
    return frame_;
  }

 protected:
  void BuildTrackingData();
  FramePtr frame_;
  DenseAlignerConfig track_cfg_;
  std::unordered_map<PyramidLevelDataPtr, ReferenceDataOnLevel> lvl_ref_map_;
 public:
  template <class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

CLOSURE_INSTANTIATE_SERIALIZATION(CLOSURE_API, TrackReference)
}  // namespace closure