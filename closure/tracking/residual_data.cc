#include "closure/tracking/residual_data.h"
#include <chrono>
#include <iostream>
#include <tbb/parallel_for.h>
#include "closure/model/frame.h"

namespace closure {

ResidualData::ResidualData(PyramidLevelDataPtr ld) {
  tracking_data = ld->GetTrackingData();
  const int len = tracking_data->size();
  valid_ids.resize(len, false);

  warped_indep.resize(Eigen::NoChange, len);  ///< warped intensity and depth
  warped_pointcloud.resize(Eigen::NoChange, len);
  inter_pointcloud.resize(Eigen::NoChange, len);  ///< interpolated points
  warped_gradient.resize(Eigen::NoChange, len);   ///< idx, idy, zdx, zdy

  residuals.resize(Eigen::NoChange, len);   ///< resi, resd
  weights.resize(Eigen::NoChange, len);     ///< wi, wd
  float last_error;   ///< average of weighted sum error
}

void ResidualData::Reset() {
  residuals.setZero();
  weights.setZero();
  last_error = 0.0f;
  valid_ids.resize(valid_ids.size(), false);
}

void ResidualData::ComputeResidualsAndWeights(
  const PyramidLevelDataPtr reference,
  const PyramidLevelDataPtr current,
  const Mat2f& cov,
  const Matrix34f& ref2cur) {
  ComputeResiduals(reference, current, ref2cur);
  ComputeWeights(reference, current, cov);
}

void ResidualData::ComputeResidualsAndWeightsSSE(
  const PyramidLevelDataPtr reference,
  const PyramidLevelDataPtr current,
  const Mat2f& cov,
  const Matrix34f& ref2cur) {
  ComputeResidualsSSE(reference, current, ref2cur);
  ComputeWeightsSSE(reference, current, cov);
}

float ResidualData::ComputeInlierRatio(const float threshold) {

  int inlier_count = 0;
  int valid_count = 0;
  for (int i = 0; i < weights.cols(); ++i) {
    if (valid_ids[i]) {
      valid_count++;
      const float w = 0.5f * weights.col(i).sum();
      if (w > threshold) inlier_count += 1;
    }
  }
  return float(inlier_count) / valid_count;
}

float ResidualData::ComputeTrackRatio(const float threshold) {

  int inlier_count = 0;
  for (int i = 0; i < weights.cols(); ++i) {
    if (valid_ids[i]) {
      const float w = 0.5f * weights.col(i).sum();
      if (w > threshold) inlier_count += 1;
    }
  }
  return float(inlier_count) / weights.cols();
}

// get an interpolated compact vector
Vec4f ResidualData::BilinearSamplePointcloud(const PyramidLevelDataPtr ldata, const Vec2f& v) {

  auto pixel_index = [&](const int x, const int y) {
    return y * ldata->width + x;
  };

  const int x0 = std::floor(v[0]);
  const int y0 = std::floor(v[1]);
  const Vec4f vcur00 = ldata->pointcloud.col(pixel_index(x0, y0));
  const Vec4f vcur10 = ldata->pointcloud.col(pixel_index(x0 + 1, y0));
  const Vec4f vcur11 = ldata->pointcloud.col(pixel_index(x0 + 1, y0 + 1));
  const Vec4f vcur01 = ldata->pointcloud.col(pixel_index(x0, y0 + 1));

  const float x_x0 = v[0] - x0;
  const float x_x1 = 1.0f - x_x0;
  const float y_y0 = v[1] - y0;
  const float y_y1 = 1.0f - y_y0;
  const Vec4f vcur_interpolated = (vcur00 * x_x1 + vcur10 * x_x0) * y_y1 +
    (vcur01 * x_x1 + vcur11 * x_x0) * y_y0;
  return vcur_interpolated;
};

Vec4f ResidualData::BilinearSampleIntensityDepth(const PyramidLevelDataPtr ldata, const Vec2f& v) {

  auto pixel_index = [&](const int x, const int y) {
    return y * ldata->width + x;
  };

  const int x0 = std::floor(v[0]);
  const int y0 = std::floor(v[1]);
  const Vec4f vcur00 = ldata->intensity_depth.col(pixel_index(x0, y0));
  const Vec4f vcur10 = ldata->intensity_depth.col(pixel_index(x0 + 1, y0));
  const Vec4f vcur11 = ldata->intensity_depth.col(pixel_index(x0 + 1, y0 + 1));
  const Vec4f vcur01 = ldata->intensity_depth.col(pixel_index(x0, y0 + 1));

  const float x_x0 = v[0] - x0;
  const float x_x1 = 1.0f - x_x0;
  const float y_y0 = v[1] - y0;
  const float y_y1 = 1.0f - y_y0;
  const Vec4f vcur_interpolated = (vcur00 * x_x1 + vcur10 * x_x0) * y_y1 +
    (vcur01 * x_x1 + vcur11 * x_x0) * y_y0;
  return vcur_interpolated;
};

// get an interpolated compact vector
Vec4f ResidualData::BilinearSampleGradient(const PyramidLevelDataPtr ldata, const Vec2f& v) {

  auto pixel_index = [&](const int x, const int y) {
    return y * ldata->width + x;
  };

  const int x0 = std::floor(v[0]);
  const int y0 = std::floor(v[1]);
  const Vec4f vcur00 = ldata->gradient.col(pixel_index(x0, y0));
  const Vec4f vcur10 = ldata->gradient.col(pixel_index(x0 + 1, y0));
  const Vec4f vcur11 = ldata->gradient.col(pixel_index(x0 + 1, y0 + 1));
  const Vec4f vcur01 = ldata->gradient.col(pixel_index(x0, y0 + 1));

  const float x_x0 = v[0] - x0;
  const float x_x1 = 1.0f - x_x0;
  const float y_y0 = v[1] - y0;
  const float y_y1 = 1.0f - y_y0;
  const Vec4f vcur_interpolated = (vcur00 * x_x1 + vcur10 * x_x0) * y_y1 +
    (vcur01 * x_x1 + vcur11 * x_x0) * y_y0;
  return vcur_interpolated;
};

void ResidualData::ComputeResiduals(const PyramidLevelDataPtr reference,
                          const PyramidLevelDataPtr current,
                          const Matrix34f& ref2cur) {
  std::chrono::high_resolution_clock::time_point tv_start, tv_end;
  tv_start = std::chrono::high_resolution_clock::now();
  
  valid_ids.resize(valid_ids.size(), false);
  residuals.setZero();

  Eigen::Matrix<float, 4, 4, Eigen::RowMajor> m44, mkt44;
  mkt44.setIdentity();
  mkt44.block<3, 4>(0, 0) = current->proj.cast<float>() * ref2cur;
  m44.setIdentity();
  m44.block<3, 4>(0, 0) = ref2cur;
  ColMat4Xf transformed_pc(4, tracking_data->pointcloud.cols());
  ColMat4Xf tproj_pc(4, tracking_data->pointcloud.cols());

  // transforming the pointcloud to current frame
  const size_t ncols = tracking_data->pointcloud.cols();
  tbb::parallel_for(tbb::blocked_range<int>(0, ncols), 
                    [&](tbb::blocked_range<int> r) { 
    for (int i = r.begin(); i != r.end(); ++i) {
      transformed_pc.col(i).noalias() = m44 * tracking_data->pointcloud.col(i);
      tproj_pc.col(i).noalias() = mkt44 * tracking_data->pointcloud.col(i);
    }
  });

  // update data if projtected transformed point is inside current image
  
  tbb::parallel_for(tbb::blocked_range<int>(0, ncols), 
                    [&](tbb::blocked_range<int> r) { 
    for (int i = r.begin(); i != r.end(); ++i) {
      const Vec4f v4 = transformed_pc.col(i);
      const Vec4f v4_proj = tproj_pc.col(i);
      const Vec2f v(v4_proj[0] / v4_proj[2], v4_proj[1] / v4_proj[2]);
      if (current->InImageWithBorder(v[0], v[1], 1)) {
        const float fx = current->proj(0, 0);
        const float fy = current->proj(1, 1);

        warped_indep.col(i) = BilinearSampleIntensityDepth(current, v);

        Vec4f ref_grad = tracking_data->gradient.col(i);
        Vec4f warped_vcur = BilinearSampleGradient(current, v);
        Vec4f ref_indep = tracking_data->intensity_depth.col(i);
        ref_indep(1) = v4[2];

        if (!std::isfinite(warped_indep(1, i)) ||
            !std::isfinite(warped_vcur(2)) ||
            !std::isfinite(warped_vcur(3))) continue;

        const float factor = 1.0f / 255.0f;
        Vec4f cufactor, reffactor;  // used to compute compact data (efficiency)
        cufactor << 0.5f * fx * factor, 0.5f * fy * factor, fx, fy;
        reffactor << 0.5f * fx * factor, 0.5f * fy * factor, 0.0f, 0.0f;

        warped_gradient.col(i) = warped_vcur.cwiseProduct(cufactor) +
                          ref_grad.cwiseProduct(reffactor);


        valid_ids[i] = true;
        warped_pointcloud.col(i) = tracking_data->pointcloud.col(i); // v4;
        residuals.col(i) = warped_indep.col(i).segment<2>(0) - ref_indep.head<2>();
        residuals(0, i) *= factor;
      }
    }
  });

  tv_end = std::chrono::high_resolution_clock::now();
  auto const mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
  printf("ComputeResiduals_TBB took:%dmilliseconds\n", mls);
}

void ResidualData::ComputeWeights(const PyramidLevelDataPtr reference,
                        const PyramidLevelDataPtr current,
                        const Mat2f& cov) {
  auto tv_start = std::chrono::high_resolution_clock::now();

  Vec2f mean_residual(0, 0);

  int count_valid = 0;
  for (int i = 0; i < valid_ids.size(); ++i) {
    if (valid_ids[i]) {
      mean_residual += residuals.col(i);
      count_valid++;
    }
  }
  mean_residual /= count_valid;

  auto calc_sigma = [&](float median_sq) -> float {
    float sigma = 1.4826f *
      (1.0f + 5.0f / (count_valid * 2.0f - 6.0f)) * sqrt(median_sq);
    sigma = 1.345f * sigma;
    return sigma * sigma;
  };

  auto sq_sigma1 = calc_sigma(mean_residual[0] * mean_residual[0]);
  auto sq_sigma2 = calc_sigma(mean_residual[1] * mean_residual[1]);

  auto calc_weight = [&](const float sq_r, const float sq_sigma) -> float {
    if (sq_r > sq_sigma) return std::sqrt(sq_sigma / sq_r);
    return 1;
  };

  weights.setZero();

  float sum_sq_error = 0.0f;
  for (int i = 0; i < valid_ids.size(); ++i) {
    if (valid_ids[i]) {
      const float e1 = residuals(0, i) * residuals(0, i);
      const float e2 = residuals(1, i) * residuals(1, i);
      const float w1 = calc_weight(e1, sq_sigma1);
      const float w2 = calc_weight(e2, sq_sigma2);
      sum_sq_error += w1 * e1 + w2 * e2;
      weights(0, i) = w1;
      weights(1, i) = w2;
    }
  }
  last_error = sum_sq_error / count_valid;

  auto tv_end = std::chrono::high_resolution_clock::now();
  auto mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
  printf("ComputeWeights took:%dmilliseconds\n", mls);
}

}  // namespace closure