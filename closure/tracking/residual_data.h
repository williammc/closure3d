#pragma once
#include <memory>
#include <vector>
#include <Eigen/Core>
#include "slick/util/mestimator.h"
#include "closure/common/datatypes.h"

namespace closure {

struct PyramidLevelData;
typedef std::shared_ptr<PyramidLevelData> PyramidLevelDataPtr;

struct TrackingData;
typedef std::shared_ptr<TrackingData> TrackingDataPtr;

struct ResidualData;
typedef std::shared_ptr<ResidualData> ResidualDataPtr;

struct ResidualData {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  ResidualData() = delete;
  ResidualData(const ResidualData&) = delete;
  ResidualData(PyramidLevelDataPtr ld);

  void Reset();

  /// get interpolated point in the pointcloud
  Vec4f BilinearSamplePointcloud(const PyramidLevelDataPtr ldata, const Vec2f& v);
  Vec4f BilinearSamplePointcloudSSE(const PyramidLevelDataPtr ldata, const Vec2f& v);

  /// get an interpolated compact vector using bilinear interpolation
  Vec4f BilinearSampleIntensityDepth(const PyramidLevelDataPtr ldata, const Vec2f& v);
  Vec4f BilinearSampleIntensityDepthSSE(const PyramidLevelDataPtr ldata, const Vec2f& v); 

  Vec4f BilinearSampleGradient(const PyramidLevelDataPtr ldata, const Vec2f& v);
  Vec4f BilinearSampleGradientSSE(const PyramidLevelDataPtr ldata, const Vec2f& v);

  typedef Eigen::Matrix<float, 3, 4, Eigen::RowMajor> Matrix34f;
  void ComputeResiduals(const PyramidLevelDataPtr reference,
                        const PyramidLevelDataPtr current,
                        const Matrix34f& ref2cur);
  void ComputeResidualsSSE(const PyramidLevelDataPtr reference,
                        const PyramidLevelDataPtr current,
                        const Matrix34f& ref2cur);

  void ComputeWeights(const PyramidLevelDataPtr reference,
                      const PyramidLevelDataPtr current,
                      const Mat2f& cov);
  void ComputeWeightsSSE(const PyramidLevelDataPtr reference,
                      const PyramidLevelDataPtr current,
                      const Mat2f& cov);

  void ComputeResidualsAndWeights(const PyramidLevelDataPtr reference,
                                  const PyramidLevelDataPtr current,
                                  const Mat2f& cov,
                                  const Matrix34f& ref2cur);
  void ComputeResidualsAndWeightsSSE(const PyramidLevelDataPtr reference,
                                  const PyramidLevelDataPtr current,
                                  const Mat2f& cov,
                                  const Matrix34f& ref2cur);

  /// #inlier/#valid
  float ComputeInlierRatio(const float threshold = 0.8f);

  /// #tracked/#points 
  float ComputeTrackRatio(const float threshold = 0.8f);
  // Properties
  TrackingDataPtr tracking_data;
  std::vector<float> valid_ids;
  ColMat4Xf warped_indep;  ///< warped intensity and depth
  ColMat4Xf warped_pointcloud; ///< transformed pointcloud
  ColMat4Xf inter_pointcloud;  ///< interpolated pointcloud
  ColMat4Xf warped_gradient;   ///< idx, idy, zdx, zdy

  ColMat2Xf residuals;   ///< resi, resd
  ColMat2Xf weights;     ///< wi, wd
  float last_error;   ///< average of weighted sum error
  int last_valid_count;
};
}  // namespace closure