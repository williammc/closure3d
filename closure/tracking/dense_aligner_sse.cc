#include "closure/tracking/dense_aligner.h"
#include <cassert>
#include <chrono>
#include <iostream>
#include <tbb/parallel_for.h>
#include <tbb/task_scheduler_init.h>
#include "slick/math/so3.h"
#include "slick/math/se3.h"
#include "closure/model/frame.h"
#include "closure/tracking/math_sse.h"
#include "closure/tracking/residual_data.h"
#include "closure/tracking/track_reference.h"

namespace closure {
  DenseAligner::AlignResult DenseAligner::AlignSSE(
    TrackReferencePtr trk_ref, FramePtr current, Eigen::Affine3d& ref2cur) {

    std::chrono::high_resolution_clock::time_point tv_start, tv_end;
    if (cfg_.debug) {
      tv_start = std::chrono::high_resolution_clock::now();
    }
    std::chrono::high_resolution_clock::time_point tv_start1, tv_end1;


    current->BuildPyramidDataSSE(cfg_.start_level + 1);

    tv_end = std::chrono::high_resolution_clock::now();

    auto mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
    printf("DenseAligner::Build data() took:%dmilliseconds\n", mls);


    assert(trk_ref->frame()->NumberOfLevels() > 0);
    assert(trk_ref->frame()->NumberOfLevels() == current->NumberOfLevels());

    const Mat3f mrot = ref2cur.matrix().block<3, 3>(0, 0).cast<float>();
    const Vec3f t = ref2cur.matrix().block<3, 1>(0, 3).cast<float>();
    slick::SO3f rot(mrot);
    slick::SE3f pose(rot, t);
    std::vector<std::vector<float>> avg_errors;
    avg_errors.resize(cfg_.start_level + 1);
    last_inlier_ratio_ = 0.0f;
    last_track_ratio_ = 0.0f;
    float last_avg_error = 0.0f;
    //current->BuildTrackingDataSSE(cfg_.td_option);
    for (int level = cfg_.start_level; level >= cfg_.end_level; --level) {
      auto reflvl = trk_ref->frame()->level(level);
      auto curlvl = current->level(level);
      tv_start1 = std::chrono::high_resolution_clock::now();
      curlvl->ComputeTrackingDataIfEmptySSE(cfg_.td_option);
      tv_end1 = std::chrono::high_resolution_clock::now();
      mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end1 - tv_start1).count();
      printf("DenseAligner:: build tracking data took:%dmilliseconds\n", mls);

      ReferenceDataOnLevel& ref_data = trk_ref->GetRefData(reflvl);
      ResidualData& rd = *trk_ref->GetRefData(reflvl).rd;

      Mat2f cov;
      cov.setIdentity();
      rd.ComputeResidualsAndWeightsSSE(reflvl, curlvl, cov,
                                    pose.get_matrix().cast<float>());

      Vec6f delta;
      std::vector<float> iter_avg_errors;
      float pre_avg_error = std::numeric_limits<float>::max();
      const int nmeas = rd.residuals.cols();
      for (int iter = 0; iter < cfg_.max_itrations[level]; ++iter) {
        tv_start1 = std::chrono::high_resolution_clock::now();
#if 0
        Mat6f JTJ = Mat6f::Zero();
        Vec6f JTE = Vec6f::Zero();

        Eigen::Matrix<float, 2, 6> J_pose;
        Vec6f J_z;
        const int nmeas = rd.residuals.cols();
        Mat2f W = Mat2f::Identity();
        for (size_t i = 0; i < nmeas; ++i) {
            if (!rd.valid_ids[i]) continue;
            W(0, 0) = rd.weights(0, i);
            W(1, 1) = rd.weights(1, i);

            const Vec4f v4 = rd.warped_pointcloud.col(i);
            compute_jacobian_uv_wrt_pose(v4, J_pose);
            compute_jacobian_z_wrt_pose(v4, J_z);

            Eigen::Matrix<float, 2, 6> J = Eigen::Matrix<float, 2, 6>::Zero();
            J.row(0) = -rd.warped_gradient.col(i).segment<2>(0).transpose() * J_pose;
            J.row(1) = -(rd.warped_gradient.col(i).segment<2>(2).transpose() * J_pose
                         - J_z.transpose());

            //cov.noalias() += J * J.transpose();
            JTJ += (J.transpose() * W) * J;
            JTE += (J.transpose() * W) * rd.residuals.col(i);
          }

        delta = Eigen::LDLT<Mat6f >(JTJ).solve(JTE);

#else
        OptMat6f JTJ_opt;
        JTJ_opt.SetZero();
        Vec6f JTE;
        JTE.setZero();
        Eigen::Matrix<float, 2, 6> J_pose;
        Vec6f J_z;
        Eigen::Matrix<float, 2, 2, Eigen::ColMajor> W;
        W.setIdentity();
        for (size_t i = 0; i < nmeas; ++i) {
          if (!rd.valid_ids[i]) continue;

          const Vec4f v4 = rd.warped_pointcloud.col(i);
          compute_jacobian_uv_wrt_pose(v4, J_pose);
          compute_jacobian_z_wrt_pose(v4, J_z);

          Eigen::Matrix<float, 2, 6, Eigen::ColMajor> J;
          J.row(0) = - rd.warped_gradient.col(i).segment<2>(0).transpose() * J_pose;
          J.row(1) = -(rd.warped_gradient.col(i).segment<2>(2).transpose() * J_pose
          - J_z.transpose());

          //cov.noalias() += J * J.transpose();

          W(0, 0) = rd.weights(0, i);
          W(1, 1) = rd.weights(1, i);
          JTJ_opt.Update(J, W);
#if 0     // TODO: check why this doesn't work
          Vec6f v6 = J.transpose() * (W * rd.residuals.col(i));
          Add(JTE, v6, 1.0f);  // JTE += JTEs[i];
#else
          JTE += J.transpose() * (W * rd.residuals.col(i));
#endif
        } 

        Mat6f JTJ_eigen;
        JTJ_opt.ToEigen(JTJ_eigen);
        delta = Eigen::LDLT<Mat6f>(JTJ_eigen).solve(JTE);

#endif
        tv_end1 = std::chrono::high_resolution_clock::now();
        mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end1 - tv_start1).count();
        printf("DenseAligner:: compute jacobians took:%dmilliseconds\n", mls);


        auto const pre_pose = pose;
        pose = slick::SE3f(delta) * pose;
        rd.ComputeResidualsAndWeightsSSE(
          reflvl, curlvl, cov, pose.get_matrix().cast<float>());
        
        if (rd.last_error > pre_avg_error || std::isnan(rd.last_error)) {
          pose = pre_pose;
          break;
        }
        pre_avg_error = rd.last_error;

        if (cfg_.debug) {
          std::cout << "iteration: " << iter
            << " rd.last_error:" << rd.last_error
            << " pose:\n" << pose.get_matrix() << std::endl;
        }
        iter_avg_errors.push_back(rd.last_error);

        if (delta.squaredNorm() <= cfg_.convergence_delta[level])
          break;
      }

      last_inlier_ratio_ = rd.ComputeInlierRatio(cfg_.inlier_threshhold);
      last_track_ratio_ = rd.ComputeTrackRatio();
      last_avg_error = pre_avg_error;

      avg_errors[level] = iter_avg_errors;

      if (cfg_.debug) {
        printf("avg errors:");
        for (auto e : iter_avg_errors) printf("%f,", e);
        printf("\n");
      }
    }

    if (cfg_.debug) {
      tv_end = std::chrono::high_resolution_clock::now();
      auto const mls = std::chrono::duration_cast<std::chrono::milliseconds>(tv_end - tv_start).count();
      printf("DenseAligner::Align() took:%dmilliseconds\n", mls);
    }
    ref2cur.matrix().block<3, 4>(0, 0) = pose.get_matrix().cast<double>();

    if (last_inlier_ratio_ > 0.7f && last_avg_error < 1.e-3)
      return AlignResult::GOOD;
    if (last_inlier_ratio_ > 0.5f && last_avg_error < 3.e-3)
      return AlignResult::OK;
    if (last_inlier_ratio_ > 0.5f && last_avg_error < 1.e-2)
      return AlignResult::BAD;
    return AlignResult::FAIL;
  }

}  // namespace closure