#include "closure/tracking/localizer.h"

#include <functional>
#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "slick/math/se3.h"
#include "closure/serialization/serialization.h"
#include "closure/tracking/dense_aligner.h"
#include "closure/tracking/track_reference.h"

namespace closure {
  using TrackResult = DenseAligner::AlignResult;

// LocalizerConfig ============================================================
template <class Archive>
void LocalizerConfig::serialize(Archive& ar, const unsigned int version) {
  ar& threading;
  ar& score_threshold;
}

// Localizer Frame ============================================================
LocalizerFrame::LocalizerFrame(FramePtr frame) {
  kf = frame;
  cv::Size size(kf->level(0)->intensity.size().width / 8,
                kf->level(0)->intensity.size().height / 8);
  ComputeSmallBlurryImage(size);
}

void LocalizerFrame::ComputeSmallBlurryImage(const cv::Size small_size, float blur) {
  cv::resize(kf->level(0)->intensity, small_blurry, small_size);
  small_blurry = small_blurry / 255.0f;
  float mean = float(cv::sum(small_blurry)[0]) / (small_blurry.cols*small_blurry.rows);
  cv::subtract(small_blurry, cv::Mat(small_blurry.rows, small_blurry.cols, CV_32FC1, mean), small_blurry);
  cv::GaussianBlur(small_blurry, small_blurry, cv::Size(7, 7), blur);
}

float LocalizerFrame::ZMSSD(LocalizerFrame& other) const {
  float ssd = 0;
  for (int r = 0; r < small_blurry.rows; ++r)
    for (int c = 0; c < small_blurry.cols; ++c) {
      const float diff = small_blurry.template at<float>(r, c) -
        other.small_blurry.template at<float>(r, c);
      ssd += diff*diff;
    }
  const float d = small_blurry.cols * small_blurry.rows;
  return ssd / d;
}

// Localizer ==================================================================
Localizer::Localizer(LocalizerConfig cfg)
  : newframe_(false), stop_(false), cfg_(cfg), do_sskf_(false) {
  Run();
}

Localizer::~Localizer() {
  Stop();
}

void Localizer::AddKeyframe(FramePtr kf) {
  if (cfg_.threading) {
    std::unique_lock<std::mutex> lock(mutex_external_);
    newframe_.store(true);
    queue_external_.push(kf);
  } else {
    Train(kf);
  }
  // note: no need unlock (the lock will do itself when going out of scope)
}

std::vector<FramePtr> Localizer::Match(FramePtr frame) {
  std::unique_lock<std::mutex> lock_internal(mutex_internal_);
  if (localizer_frames_.empty())
    return std::vector<FramePtr>();
  std::vector<float> zmssd_scores;
  std::map<float, LocalizerFramePtr> score_lf_map;

  LocalizerFrame try_lf(frame);
  for (auto lf : localizer_frames_) {
    const float score = try_lf.ZMSSD(*lf);
    score_lf_map[score] = lf;
    zmssd_scores.push_back(score);
  }
  std::sort(zmssd_scores.begin(), zmssd_scores.end());

  std::vector<FramePtr> best_matches;  // decreasing
  for (auto score : zmssd_scores)
    best_matches.push_back(score_lf_map[score]->kf);
  return best_matches;
}

void Localizer::ShouldSwitchKeyframe(FramePtr frame,
                                     FramePtr kf) {
  if (!cfg_.threading) {
    auto kfs = Match(frame);

    if (!kfs.empty() && kfs.front() != kf) {
      DenseAlignerConfig cfg(4, 2);
      DenseAligner aligner(cfg);
      TrackReferencePtr ref(new TrackReference(kfs.front(), cfg));
      Eigen::Affine3d ref2frame;
      TrackResult res = aligner.AlignSSE(ref, frame, ref2frame);
      // add loop-closure constraint
      if (res == TrackResult::GOOD) {
        Signal()(kfs.front());
      }
    }
  } else {
    do_sskf_.store(true);
    switch_frame_ = frame;
    switch_kf_ = kf;
  }
}

std::pair<bool, Eigen::Affine3d> Localizer::Localize(FramePtr frame) {
  std::unique_lock<std::mutex> lock_internal(mutex_internal_);
  if (localizer_frames_.empty())
    return std::make_pair(false, Eigen::Affine3d());
  std::vector<float> zmssd_scores;
  std::map<float, LocalizerFramePtr> score_lf_map;

  LocalizerFrame try_lf(frame);
  for (auto lf : localizer_frames_) {
    const float score = try_lf.ZMSSD(*lf);
    score_lf_map[score] = lf;
    zmssd_scores.push_back(score);
  }
  std::sort(zmssd_scores.begin(), zmssd_scores.end());

  if (zmssd_scores.front() > cfg_.score_threshold) 
    return std::make_pair(false, Eigen::Affine3d());

  std::vector<Eigen::Affine3d, Eigen::aligned_allocator<Eigen::Affine3d>> try_poses;
  try_poses.resize(std::min(3, int(zmssd_scores.size())));
  std::vector<TrackResult> results(try_poses.size(), TrackResult::FAIL);

  auto try_localize = [&](int index) {
    auto reference = score_lf_map[zmssd_scores.at(index)]->kf;
    DenseAlignerConfig cfg(4, 2);
    DenseAligner aligner(cfg);
    TrackReferencePtr trk_ref(new TrackReference(reference, cfg));
    results[index] = aligner.AlignSSE(trk_ref, frame, try_poses[index]);
    try_poses[index] = try_poses[index] * reference->pose().inverse();
  };

  try_localize(0);

  if (results[0] != TrackResult::FAIL)
    return std::make_pair(true, try_poses[0]);

 return std::make_pair(false, Eigen::Affine3d());
}

void Localizer::Run() {
  auto loop_func = [&]() {
    while (!stop_.load()) {
      std::cout << "looping in relocalizer\n";

      if (do_sskf_.load()) {
        do_sskf_.store(false);
        auto frame = switch_frame_;
        auto kf = switch_kf_;

        auto kfs = Match(frame);
        if (!kfs.empty() && kfs.front() != kf) {
          Signal()(kfs.front());
        }
      }
      
      // std::defer_lock make it initially unlocked
      std::unique_lock<std::mutex> lock_external(mutex_external_, std::defer_lock);
      lock_external.lock();
      if (!newframe_.load()) {
        lock_external.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      } else {
        std::unique_lock<std::mutex> lock_internal(mutex_internal_);
        while (queue_external_.size() != 0) {
          queue_internal_.push(queue_external_.front());
          queue_external_.pop();
        }
        newframe_.store(false);
        lock_external.unlock();

        while (queue_internal_.size() > 0) {
          FramePtr kf = queue_internal_.front();
          queue_internal_.pop();
          Train(kf);
        }
        lock_internal.unlock();
      }
    }
  };
  if (cfg_.threading)  // start new thread for relocalization
    localizer_thread_ = std::thread(loop_func);
}

void Localizer::Stop() {
  if (cfg_.threading) {
    stop_.store(true);
    if (localizer_thread_.joinable())
      localizer_thread_.join();
  }
}

void Localizer::Train(FramePtr kf) {
  std::shared_ptr<LocalizerFrame> relocframe(new LocalizerFrame(kf));
  localizer_frames_.push_back(relocframe);
}

void Localizer::Clear(){
  std::unique_lock<std::mutex> lock_internal(mutex_internal_);
  if(!localizer_frames_.empty())
    localizer_frames_.clear();
  while(queue_internal_.size()>0)
    queue_internal_.pop();
  std::unique_lock<std::mutex> lock_external(mutex_external_);
  while(queue_external_.size()>0)
    queue_external_.pop();
}

template <class Archive>
void Localizer::serialize(Archive& ar, const unsigned int version) {
  ar& cfg_;
  ar& queue_external_;
  ar& queue_internal_;
  ar& localizer_frames_;
  ar& switch_frame_;
  ar& switch_kf_;
  ar& signal_;
}

CLOSURE_INSTANTIATE_SERIALIZATION_T(Localizer)
}  // namespace closure