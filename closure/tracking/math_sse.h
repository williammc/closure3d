// This file was initially written by Jakob Engel, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.
#pragma once

#include <Eigen/Core>

namespace closure {

/// A 6x6 self adjoint matrix with optimized SSE code 
/// TODO: replacing this code from DVO libs
struct OptimizedSelfAdjointMatrix6x6f {
  OptimizedSelfAdjointMatrix6x6f() = default;

  /// @alpha must be column major order
  void Update(const Eigen::Matrix<float, 6, 1>& u, const float& alpha);

  /// @u and @alpha must be column major order
  void Update(const Eigen::Matrix<float, 2, 6>& u, const Eigen::Matrix2f& alpha);

  void operator +=(const OptimizedSelfAdjointMatrix6x6f& other);

  
  void SetZero() {
    for(size_t idx = 0; idx < Size; idx++)
      data[idx] = 0.0f;
  }

  void ToEigen(Eigen::Matrix<float, 6, 6>& m) const;
private:
  enum {
    Size = 24
  };
  EIGEN_ALIGN16 float data[Size];
};

typedef OptimizedSelfAdjointMatrix6x6f OptMat6f;

void AddOuterProduct(Eigen::Matrix<float, 6, 6>& mat,
                     const Eigen::Matrix<float, 6, 1>& vec,
                     const float& scale);

void AddOuterProduct(Eigen::Matrix<double, 6, 6>& mat,
                     const Eigen::Matrix<double, 6, 1>& vec,
                     const double& scale);

/// vec = vec + other*scale;
void Add(Eigen::Matrix<float, 6, 1>& vec,
         const Eigen::Matrix<float, 6, 1>& other,
         const float& scale);
void Add(Eigen::Matrix<double, 6, 1>& vec,
         const Eigen::Matrix<double, 6, 1>& other,
         const double& scale);

}  // namespace closure
