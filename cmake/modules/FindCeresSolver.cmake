find_path(CeresSolver_INCLUDE_DIR 
            NAMES ceres/ceres.h 
            PATHS /usr/include 
                  /usr/local/include
                  /opt/ros/include
                  ${CeresSolver_INCLUDE_DIR})

find_library(CeresSolver_LIBRARY 
               NAMES ceres 
               PATHS [/usr/lib /usr/local/lib] 
                     /opt/ros/lib
                     ${CeresSolver_INCLUDE_DIR}/../lib)

IF (CeresSolver_INCLUDE_DIR AND CeresSolver_LIBRARY)
   SET(CeresSolver_FOUND TRUE)
ENDIF (CeresSolver_INCLUDE_DIR AND CeresSolver_LIBRARY)

IF (CeresSolver_FOUND)
   IF (NOT CeresSolver_FIND_QUIETLY)
      MESSAGE(STATUS "Found CeresSolver: ${CeresSolver_INCLUDE_DIR}")
      MESSAGE(STATUS "Found CeresSolver: ${CeresSolver_LIBRARY}")
   ENDIF (NOT CeresSolver_FIND_QUIETLY)
ELSE (CeresSolver_FOUND)
   IF (CeresSolver_FIND_REQUIRED)
      MESSAGE(STATUS "Could not find CeresSolver")
   ENDIF (CeresSolver_FIND_REQUIRED)
ENDIF (CeresSolver_FOUND)
