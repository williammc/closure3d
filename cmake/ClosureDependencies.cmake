cmaker_print_status("Setup required dependencies for Closure tracking library")

##==============================================================================
## Boost
if(WIN32)
  set(Boost_USE_STATIC_LIBS  ON)
endif(WIN32)

find_package(Boost REQUIRED COMPONENTS system filesystem serialization bzip2)

if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  set(Closure_EXTERNAL_LIBS ${Closure_EXTERNAL_LIBS} ${Boost_LIBRARIES})
  link_directories(${Boost_LIBRARY_DIR})
  set(Closure_HAVE_SERIALIZATION ON)
  add_definitions(-DClosure_HAVE_SERIALIZATION)
  cmaker_print_status("Boost libs: ${Boost_LIBRARIES}")
endif(Boost_FOUND)

##==============================================================================
##  OpenCV
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
list(APPEND Closure_EXTERNAL_LIBS ${OpenCV_LIBRARIES})
cmaker_print_status("OpenCV include dirs:${OpenCV_INCLUDE_DIRS}")
cmaker_print_status("OpenCV libs:${OpenCV_LIBRARIES}")
get_filename_component(OpenCV_BINARY_DIR "${OpenCV_LIB_DIR}/../bin" ABSOLUTE)
list(APPEND THE_DEPEDENCIES_BINARY_PATHS ${OpenCV_BINARY_DIR})

## TBB =========================================================================
## TBB
# set(TBB_ARCH_PLATFORM "ia32")
# set(TBB_COMPILER "vc12")
# set(TBB_INSTALL_DIR "D:/deps_msvc_common/tbb42_20140601oss")
find_package(TBB)
include_directories(${TBB_INCLUDE_DIR})
link_directories(${TBB_LIBRARY_DIRS})
list(APPEND Closure_EXTERNAL_LIBS ${TBB_LIBRARIES})
set(TBB_BINARY_DIR ${TBB_INSTALL_DIR}/bin/${TBB_ARCH_PLATFORM}/${TBB_COMPILER})
list(APPEND THE_DEPEDENCIES_BINARY_PATHS ${TBB_BINARY_DIR})

cmaker_print_status("TBB_LIBRARIES:${TBB_LIBRARIES}")
cmaker_print_status("TBB_BINARY_DIR:${TBB_BINARY_DIR}")

if(TBB_FOUND)
  set(Closure_HAVE_TBB ON)
  add_definitions(-DClosure_HAVE_TBB)
endif()

## Ceres Solver ================================================================
find_package(Ceres REQUIRED)
include_directories(${CERES_INCLUDE_DIRS})
list(APPEND Closure_EXTERNAL_LIBS ${CERES_LIBRARIES})
link_directories(${CERES_INCLUDE_DIRS}/../lib)